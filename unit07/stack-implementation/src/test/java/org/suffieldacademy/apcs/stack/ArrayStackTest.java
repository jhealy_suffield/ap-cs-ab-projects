// $Id: ArrayStackTest.java 732 2020-01-30 14:34:50Z jhealy $
package org.suffieldacademy.apcs.stack;

import org.junit.Test;
import java.util.EmptyStackException;


/**
 * <p>
 * Subclass of StackTest that specifies all tests to be run on an ArrayStack.
 * </p>
 *
 * @author Jason Healy
 */
public class ArrayStackTest extends StackTest {

    @Override
    public void beforeEachTest() {
        s = new ArrayStack<String>();
    }

    // We "repeat" the test declarations here so they are visible as Tests
    // to the test harness (allowing us to discover and run them individually)

    @Override
    @Test(expected = EmptyStackException.class)
    public void popEmpty() {
        super.popEmpty();
    }

    @Override
    @Test(expected = EmptyStackException.class)
    public void peekEmpty() {
        super.peekEmpty();
    }

    @Override
    @Test
    public void isEmpty() {
        super.isEmpty();
    }

    @Override
    @Test
    public void basicPushPop() {
        super.basicPushPop();
    }

    @Override
    @Test
    public void pushPopEmpty() {
        super.pushPopEmpty();
    }

    @Test(expected = IllegalStateException.class)
    public void causeOverflow() {
        for (int i = 0; i < 10000; i += 1) {
            s.push(""+i);
        }
    }

} // end of class ArrayStackTest
