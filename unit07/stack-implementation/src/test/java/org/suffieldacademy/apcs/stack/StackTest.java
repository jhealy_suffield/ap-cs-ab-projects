// $Id: StackTest.java 732 2020-01-30 14:34:50Z jhealy $
package org.suffieldacademy.apcs.stack;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;
// We let inherited classes declare tests
//import org.junit.Test;


/**
 * <p>
 * <b>StackTest</b> checks the implementation of Stack to ensure
 * that it performs all operations correctly.
 * </p>
 *
 * <p>This class is abstract, but defines all of the test methods.
 * Concrete classes select the implementation (List or Array) to
 * test, and rely on the inherited methods to do the work.
 *
 * @author Jason Healy
 */
public abstract class StackTest {

    /** Stack interface variable, to be initialized by subclass. */
    protected Stack<String> s = null;

    /** Subclass should use this method to initialize the variable above. */
    @Before
    public abstract void beforeEachTest();

    /** Set the variable back to null after each test. */
    @After
    public void afterEachTest() {
        s = null;
    }

    public void popEmpty() {
        s.pop();
    }


    public void peekEmpty() {
        s.peekTop();
    }


    public void isEmpty() {
        assertTrue("Empty stack should be empty", s.isEmpty());
        s.push("A");
        assertFalse("Stack after push should not be empty", s.isEmpty());
    }


    public void basicPushPop() {

        String a = "A";
        s.push(a);
        assertEquals("Just-pushed item should be returned by peekTop()",
                     a, s.peekTop());
        assertEquals("Just-pushed item should be returned by pop()",
                     a, s.pop());
    }


    public void pushPopEmpty() {
        s.push("A");
        s.pop();
        assertTrue("Stack should be empty after push-pop combo", s.isEmpty());
    }


} // end of class StackTest
