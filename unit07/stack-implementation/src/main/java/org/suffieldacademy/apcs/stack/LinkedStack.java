package org.suffieldacademy.apcs.stack;

import java.util.EmptyStackException;


/**
 * <p>
 * <b>LinkedStack</b> implements the Stack interface using a dynamic
 * linked data structure.
 * </p>
 *
 * @author Your Name
 *
 */
public class LinkedStack<E> implements Stack<E> {

    // Your private variable(s) and constructor(s) here


    /**
     * postcondition: returns true if stack is empty;
     *                otherwise, returns false
     */
    public boolean isEmpty() {
        return false; // REPLACE WITH YOUR CODE
    }


    /**
     * precondition:  stack is [e1, e2, ..., en] with n >= 0
     * postcondition: stack is [e1, e2, ..., en, x]
     * no exceptions are thrown, as linked stacks do not have a max capacity
     */
    public void push(E x) {
        // REPLACE WITH YOUR CODE
    }


    /**
     * precondition:  stack is [e1, e2, ..., en] with n >= 1
     * postcondition: stack is [e1, e2, ..., e(n-1)]; returns en
     * exceptions: throws an EmptyStackException if the stack is empty
    */
    public E pop() {
        return null; // REPLACE WITH YOUR CODE
    }


    /**
     * precondition:  stack is [e1, e2, ..., en] with n >= 1
     * postcondition: returns en
     * exceptions: throws an EmptyStackException if the stack is empty
     */
    public E peekTop() {
        return null; // REPLACE WITH YOUR CODE
    }

    /**
     * <p>
     * Returns an array representation of the list.  The array should be
     * sized to exactly fit all the items.  If the list is empty, a zero-
     * length array should be returned.
     * </p>
     *
     * @return Object[] An array representation of the list
     */
    public Object[] toArray() {
        return null; // REPLACE WITH YOUR CODE
    }

    /**
     *
     * <p>
     * <code>Node</code> is a private inner class used by the linked
     * list to store its data and the list structure.  Because it is
     * declared inside the LinkedStack class, you can directly access
     * the private instance variables of the class.
     * </p>
     *
     * <p>
     * Because a stack only grows in one direction, a singly-linked list
     * is all that is necessary.
     * </p>
     *
     * @author Jason Healy
     *
     * @param <E> The type of data held by the node; must match the
     * enclosing list
     */
    private class Node<E> {

        private E data;
        private Node<E> next;

        /**
         * <p>
         * Creates a new list node holding the given data, and referring to
         * the given next nodes in the list.
         * </p>
         *
         * @param data The object to store in this node
         * @param next The next node in the list
         */
        public Node(E data, Node<E> next) {
            this.data = data;
            this.next = next;
        }

        // No methods are necessary; you may access the private fields
        // directly by using "node.data" syntax.

    } // end of class Node

} // end of class LinkedStack
