package org.suffieldacademy.apcs.stack;


/**
 * <p>
 * <b>Stack</b> interface from the AP subset.  Based on a simplified
 * version of java.util.Stack.
 * </p>
 *
 * Last Modified $Date: 2020-01-30 09:34:50 -0500 (Thu, 30 Jan 2020) $ by $Author: jhealy $
 *
 */
public interface Stack<E> {

    /**
     * postcondition: returns true if stack is empty;
     *                otherwise, returns false
     */
    public boolean isEmpty();

    /**
     * precondition:  stack is [e1, e2, ..., en] with n >= 0
     * postcondition: stack is [e1, e2, ..., en, x]
     * exceptions: throws an IllegalStateException if the stack is full
     *             (only for stacks that have a maximum capacity)
     */
    public void push(E x);

    /**
     * precondition:  stack is [e1, e2, ..., en] with n >= 1
     * postcondition: stack is [e1, e2, ..., e(n-1)]; returns en
     * exceptions: throws an EmptyStackException if the stack is empty
    */
    public E pop();

    /**
     * precondition:  stack is [e1, e2, ..., en] with n >= 1
     * postcondition: returns en
     * exceptions: throws an EmptyStackException if the stack is empty
     */
    public E peekTop();

} // end of interface Stack
