package org.suffieldacademy.apcs.stack;

import java.util.EmptyStackException;


/**
 * <p>
 * <b>ArrayStack</b> implements the Stack interface using an
 * array-backed data structure.
 * </p>
 *
 * @author Your Name
 *
 */
public class ArrayStack<E> implements Stack<E> {

    // Your private variable(s) and constructor(s) here


    /**
     * postcondition: returns true if stack is empty;
     *                otherwise, returns false
     */
    public boolean isEmpty() {
        return false; // REPLACE WITH YOUR CODE
    }


    /**
     * precondition:  stack is [e1, e2, ..., en] with n >= 0
     * postcondition: stack is [e1, e2, ..., en, x]
     * exception: throws an IllegalStateException if the stack is full
     */
    public void push(E x) {
        // REPLACE WITH YOUR CODE
    }


    /**
     * precondition:  stack is [e1, e2, ..., en] with n >= 1
     * postcondition: stack is [e1, e2, ..., e(n-1)]; returns en
     * exceptions: throws an EmptyStackException if the stack is empty
    */
    public E pop() {
        return null; // REPLACE WITH YOUR CODE
    }


    /**
     * precondition:  stack is [e1, e2, ..., en] with n >= 1
     * postcondition: returns en
     * exceptions: throws an EmptyStackException if the stack is empty
     */
    public E peekTop() {
        return null; // REPLACE WITH YOUR CODE
    }


} // end of class ArrayStack
