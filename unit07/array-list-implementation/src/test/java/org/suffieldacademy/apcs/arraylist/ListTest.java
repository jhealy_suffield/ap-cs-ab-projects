// $Id: ListTest.java 733 2020-02-03 04:07:26Z jhealy $
package org.suffieldacademy.apcs.arraylist;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;

import java.util.ListIterator;


/**
 * <p>
 * <b>ListTest</b> checks the implementation of List to ensure
 * that it performs all operations correctly.
 * </p>
 *
 * @author Jason Healy
 */
public class ListTest {

    @Test
    public void testEmpty() {

        List<String> l = new ArrayList<String>();

        assertEquals("A new list should have a size of zero",
                     0, l.size());

        try {
            l.get(0);
            fail("Empty list should not allow get(0)");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("get(0) on empty list threw unexpected exception: " + e);
        }

        try {
            l.get(-1);
            fail("List should not allow get(-1)");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("get(-1) threw unexpected exception: " + e);
        }

        try {
            l.get(l.size() + 1);
            fail("List should not allow get(list.size()+1)");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("get(list.size()+1) threw unexpected exception: " + e);
        }

    }


    @Test
    public void testOne() {

        List<String> l = new ArrayList<String>();
        String zero = "Zero";

        // add
        try {
            assertTrue("Successfully adding an element should return true",
                    l.add(zero));
        }
        catch (Exception e) {
            fail("add() threw unexpected exception: " + e);
        }

        assertEquals("List after adding single element has wrong size",
                     1, l.size());

        String zeroGet = null;

        // get added
        try {
            zeroGet = l.get(0);
        }
        catch (Exception e) {
            fail("get(0) threw unexpected exception: " + e);
        }

        assertSame("List after adding single element did not return added element in get()",
                     zero, zeroGet);


        // set
        zero = "ZeroNew";

        try {
            l.set(zero, 0);
        }
        catch (Exception e) {
            fail("set(0) threw unexpected exception: " + e);
        }

        assertEquals("List after modifying single element has wrong size",
                     1, l.size());

        zeroGet = null;

        // get set
        try {
            zeroGet = l.get(0);
        }
        catch (Exception e) {
            fail("get(0) threw unexpected exception: " + e);
        }

        assertSame("List after modifying single element did not return added element in get()",
                     zero, zeroGet);


        // remove
        try {
            l.remove(0);
        }
        catch (Exception e) {
            fail("remove(0) threw unexpected exception: " + e);
        }

        assertEquals("List after removing single element has wrong size",
                     0, l.size());

        zeroGet = null;

        // get remove
        try {
            zeroGet = l.get(0);
            fail("get(0) after removing only element should throw exception");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("get(0) on removed list threw unexpected exception: " + e);
        }

    }

    @Test
    public void testMany() {
        ArrayList<String> l = new ArrayList<String>();
        String[] a = new String[] {"Zero", "One", "Two", "Three", "Four",
                                   "Five", "Six", "Seven", "Eight", "Nine" };

        for (int i = 0; i < a.length; i++) {
            l.add(a[i]);
        }

        assertEquals("List after adding multiple elements has wrong size",
                     a.length, l.size());

        for (int i = 0; i < a.length; i++) {
            assertEquals("List item wrong at position " + i,
                         a[i], l.get(i));
        }
    }

    // Helper method: generate an ArrayList with some elements
    public static final String[] CANONICAL =
        new String[] {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};

    public java.util.ArrayList<String> makeCanonical() {
        java.util.ArrayList<String> l = new java.util.ArrayList<String>();

        for (String s : CANONICAL) {
            l.add(s);
        }

        return l;
    }

    public ArrayList<String> makeList() {
        ArrayList<String> l = new ArrayList<String>();

        for (String s : CANONICAL) {
            l.add(s);
        }

        return l;
    }

    public void assertCanonical(String msg,
                                java.util.ArrayList<String> c,
                                ArrayList<String> l) {
        for (int i = 0; i < c.size(); i++) {
            assertEquals(msg + " at position " + i, c.get(i), l.get(i));
        }
    }


    public void addAtPosition(int p) {
        java.util.ArrayList<String> c = makeCanonical();
        ArrayList<String> l = makeList();

        c.add(p, "X");
        l.add("X", p);

        assertEquals("List item wrong after add at position " + p,
                     c.get(p), l.get(p));

        assertEquals("List size wrong after add at position " + p,
                     c.size(), l.size());

        assertCanonical("After add at position " + p
                        + " independent value incorrect",
                        c, l);
    }

    public void setAtPosition(int p) {
        java.util.ArrayList<String> c = makeCanonical();
        ArrayList<String> l = makeList();

        String cprev = c.set(p, "X");
        String lprev = l.set("X", p);

        assertEquals("Incorrect former item after set at position " + p,
                     cprev, lprev);

        assertEquals("List item wrong after set at position " + p,
                     c.get(p), l.get(p));

        assertEquals("List size wrong after set at position " + p,
                     c.size(), l.size());

        assertCanonical("After set at position " + p
                        + " independent value incorrect",
                        c, l);
    }

    public void removeAtPosition(int p) {
        java.util.ArrayList<String> c = makeCanonical();
        ArrayList<String> l = makeList();

        String cprev = c.remove(p);
        String lprev = l.remove(p);

        assertEquals("Incorrect former item after remove at position " + p,
                     cprev, lprev);

        assertEquals("List item wrong after remove at position " + p,
                     c.get(p), l.get(p));

        assertEquals("List size wrong after remove at position " + p,
                     c.size(), l.size());

        assertCanonical("After remove at position " + p
                        + " independent value incorrect",
                        c, l);
    }


    @Test
    public void addAtStart() {
        addAtPosition(0);
    }

    @Test
    public void setAtStart() {
        setAtPosition(0);
    }

    @Test
    public void removeAtStart() {
        removeAtPosition(0);
    }

    @Test
    public void addAtEnd() {
        addAtPosition(CANONICAL.length);
    }

    @Test
    public void setAtEnd() {
        addAtPosition(CANONICAL.length-1);
    }

    @Test
    public void removeAtEnd() {
        addAtPosition(CANONICAL.length-1);
    }

    @Test
    public void randomStressTest() {
        ArrayList<String> l = new ArrayList<String>();
        java.util.ArrayList<String> c = new java.util.ArrayList<String>();

        for (int i = 0; i < CANONICAL.length; i++) {
            l.add(CANONICAL[i]);
            c.add(CANONICAL[i]);
        }

        assertEquals("List size incorrect after adding test elements",
                     CANONICAL.length, l.size());

        java.util.Random r = new java.util.Random();

        for (int i = 0; i < 100; i++) {
            int pos1 = r.nextInt(l.size());
            String ran1 = ""+r.nextInt(1000);

            l.add(ran1, pos1);
            c.add(pos1, ran1);

            assertEquals("List after random pos add does not have correct size",
                         c.size(), l.size());

            assertCanonical("After random pos add at position " + pos1
                        + " does not contain correct element",
                            c, l);

            int rem1 = r.nextInt(l.size());
            l.remove(rem1);
            c.remove(rem1);

            assertEquals("List after random pos remove does not have correct size",
                         c.size(), l.size());

            assertCanonical("After random pos remove at position " + rem1
                        + " does not contain correct element",
                            c, l);

        }
    }


    @Test
    public void testIterator() {
        ListIterator c = makeCanonical().listIterator();
        ListIterator l = makeList().listIterator();

        assertFalse("ListIterator hasPrevious() returned true at start of list",
                    l.hasPrevious());

        // forward direction
        while (c.hasNext()) {
            assertTrue("ListIterator hasNext() returned false when next items still remain",
                       l.hasNext());
            assertEquals("ListIterator nextIndex() incorrect",
                         c.nextIndex(), l.nextIndex());
            assertEquals("ListIterator previousIndex() incorrect",
                         c.previousIndex(), l.previousIndex());
            assertEquals("ListIterator next() value incorrect",
                         c.next(), l.next());
        }

        assertFalse("ListIterator hasNext() returned true end of list",
                    l.hasNext());

        // reverse direction
        while (c.hasPrevious()) {
            assertTrue("ListIterator hasPrevious() returned false when previous items still remain",
                       l.hasPrevious());
            assertEquals("ListIterator nextIndex() incorrect",
                         c.nextIndex(), l.nextIndex());
            assertEquals("ListIterator previousIndex() incorrect",
                         c.previousIndex(), l.previousIndex());
            assertEquals("ListIterator next() value incorrect",
                         c.previous(), l.previous());
        }

        assertFalse("ListIterator hasPrevious() returned true at start of list",
                    l.hasPrevious());
    }


} // end of class ListTest
