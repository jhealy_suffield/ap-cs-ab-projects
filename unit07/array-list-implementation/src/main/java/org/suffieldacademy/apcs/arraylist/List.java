package org.suffieldacademy.apcs.arraylist;

import java.util.Iterator;
import java.util.ListIterator;


/**
 * <p>
 * <b>List</b> interface from the AP subset.  Based on a simplified
 * version of java.util.list.
 * </p>
 *
 * Last Modified $Date: 2020-01-30 09:34:50 -0500 (Thu, 30 Jan 2020) $ by $Author: jhealy $
 *
 */
public interface List<E> extends Iterable<E> {


    /**
     * <p>
     * Returns the number of elements in this list (zero if it is empty).
     * </p>
     *
     * @return int The number of elements in the list
     */
    public int size();


    /**
     * <p>
     * Appends the given element to the end of the list.
     * </p>
     *
     * @param obj The element to add
     *
     * @return boolean True if the item was successfully added
     */
    public boolean add(E obj);


    /**
     * <p>
     * Inserts the given item at the indicated location in the list.
     * </p>
     *
     * @param obj The element to insert
     * @param index The index to insert at (greater than -1, less than or equal to size)
     *
     * @throws IllegalArgumentException If the index is out of range
     */
    public void add(E obj, int index);


    /**
     * <p>
     * Returns the element at the given index.
     * </p>
     *
     * @param index The index of the element to return (between zero and size-1)
     *
     * @return E The element at the given index
     *
     * @throws IllegalArgumentException If the index is out of range
     */
    public E get(int index);


    /**
     * <p>
     * Replaces the element at the given index with the element provided.
     * </p>
     *
     * @param obj The new element to store in position index
     * @param index The index of the element to replace (between zero and size-1)
     *
     * @return E The previous value stored at position index
     *
     * @throws IllegalArgumentException If the index is out of range
     */
    public E set(E obj, int index);


    /**
     * <p>
     * Removes the element at the given position and returns it.  All
     * elements to the right of the given position (index + 1 and above)
     * are shifted down one position to fill the space from the removed
     * element.
     * </p>
     *
     * @param index The index of the element to remove (between zero and size-1)
     *
     * @return E The value of the removed element
     *
     * @throws IllegalArgumentException If the index is out of range
     */
    public E remove(int index);


    /**
     * <p>
     * Returns an interator over the elements of this list.
     * </p>
     *
     * @return Iterator<E> An iterator over the elements of the list in proper sequence.
     */
    public Iterator<E> iterator();


    /**
     * <p>
     * Returns a list interator over the elements of this list.
     * </p>
     *
     * @return ListIterator<E> A list iterator over the elements of the list in proper sequence.
     */
    public ListIterator<E> listIterator();


} // end of interface List
