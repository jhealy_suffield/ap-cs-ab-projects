package org.suffieldacademy.apcs.bst;


/**
 * <p>
 * <b>TreeMap</b> implements the Map interface using a Binary Search Tree
 * to sort and store nodes.
 * </p>
 *
 * <p>
 * A Map is a data structure that stores pairs of objects (a "key" and
 * its associated "value").  They keys must be unique; there cannot be
 * duplicates.  The data structure allows for insertion, removal, and
 * querying for particular keys (and thus their associated values).
 * </p>
 *
 * <p>
 * Note that we've provided a "TreeUtil" class with some helper methods
 * that can print out your trees to help you debug them.  The
 * printLevel() and printOrdered() methods can take either a node or
 * an entire tree and format them as a String (or print to System.out)
 * so you can see what your trees look like in memory.
 * </p>
 *
 * @author Your Name
 */
public class TreeMap<K extends Comparable<? super K>,V> implements Map<K,V> {

    /** Root of the binary search tree */
    private TreeMapNode<K,V> root;

    /** Size of the collection of objects */
    private int size;


    /**
     * <p>
     * Constructor: create a new empty map.
     * </p>
     */
    public TreeMap() {
        root = null;
        size = 0;
    }


    /**
     * <p>
     * Return the root of the binary search tree.  Used to expose the
     * inner structure of the map to other classes (such as TreeSet)
     * so they can access nodes directly.
     * </p>
     *
     * @return TreeMapNode<K,V> The root of the BST that backs this map
     */
    protected TreeMapNode<K,V> getRoot() {
        return root;
    }


    /**
     * @see Map#put(K, V) Map Interface Specification for put()
     */
    public V put(K key, V value) {
        // FIXME (only here so the code compiles; you should return a value)
        return null;
    }



    /**
     * @see Map#get(K) Map Interface Specification for get()
     */
    public V get(K key) {
        // FIXME (only here so the code compiles; you should return a value)
        return null;
    }


    /**
     * @see Map#remove(K) Map Interface Specification for remove()
     */
    public V remove(K key) {
        // FIXME (only here so the code compiles; you should return a value)
        return null;
    }


    /**
     * @see Map#containsKey(K) Map Interface Specification for containsKey()
     */
    public boolean containsKey(K key) {
        // FIXME (only here so the code compiles; you should return a value)
        return false;

        // hint: get(key) != null is NOT the same as containing a key!!
    }


    /**
     * @see Map#size() Map Interface Specification for size()
     */
    public int size() {
        // FIXME (only here so the code compiles; you should return a value)
        return -1;
    }


    /**
     * @see Map#keySet() Map Interface Specification for keySet()
     */
    public Set<K> keySet() {
        // Create a new Set using this Map as the backing store.
        // Then the set can just iterate over the keys directly.
        return new TreeSet<K>(this);
    }


} // end of class TreeMap
