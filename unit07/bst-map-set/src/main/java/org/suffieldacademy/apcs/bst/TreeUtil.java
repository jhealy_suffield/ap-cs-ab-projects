package org.suffieldacademy.apcs.bst;


import java.util.Queue;
import java.util.LinkedList;

import java.io.PrintStream;


/**
 * <p>
 * <b>TreeUtil</b> provides some methods to help testers and students
 * debug their trees.  You do NOT need to modify this code in any way;
 * it is provided to help you debug your TreeMap and TreeSet implementations.
 * </p>
 *
 * <p>
 * The printed trees are formatted for the terminal so that the bottom (largest)
 * level is the most tightly spaced.  You may need to adjust your window width to
 * fit large trees.  As an example, here is a sample tree output of a four-level
 * BST with the key "E" and value "5" in the root node.  The nodes are spaced to
 * show the left/right relationship, but no lines are drawn between nodes (you
 * have to imagine them since it's hard to draw lines on the console).
 * </p>
 *
 * <pre>
 *                      [E/5]
 *
 *          [B/2]                   [H/8]
 *
 *    [A/1]       [D/4]       [F/6]       [I/9]
 *
 * [ / ] [ / ] [E/5] [ / ] [ / ] [G/7] [ / ] [ / ]
 * </pre>
 *
 * @author Jason Healy
 * @internal Not student edited
 */
public class TreeUtil {

    /**
     * <p>
     * Returns a string representation of a level-order traversal of the tree
     * </p>
     *
     * @param r The tree to print
     *
     * @return String A multi-line 2D representation of the tree
     */
    public static <K extends Comparable<? super K>,V> String printLevel(TreeMap<K,V> m) {
        return printLevel(m.getRoot());
    }


    /**
     * <p>
     * Prints a level-order traversal of the tree to the given PrintStream.
     * </p>
     *
     * @param r The tree to print
     * @param s The stream to print to (such as System.out)
     */
    public static <K extends Comparable<? super K>,V> void printLevel(TreeMap<K,V> m, PrintStream s) {
        s.append(printLevel(m.getRoot()));
    }


    /**
     * <p>
     * Prints a level-order traversal of the tree to the given PrintStream.
     * </p>
     *
     * @param r The root of the tree to print
     * @param s The stream to print to (such as System.out)
     */
    public static <K extends Comparable<? super K>,V> void printLevel(TreeMapNode<K,V> r, PrintStream s) {
        s.append(printLevel(r));
    }


    /**
     * <p>
     * Returns a string representation of a level-order traversal of the tree
     * </p>
     *
     * @param r The root of the tree to print
     *
     * @return String A multi-line 2D representation of the tree
     */
    public static <K extends Comparable<? super K>,V> String printLevel(TreeMapNode<K,V> r) {
        StringBuilder sb = new StringBuilder(512);
        printLevel(r, sb);
        return sb.toString();
    }


    /**
     * <p>
     * Returns a string representation of an in-order traversal of the tree
     * </p>
     *
     * @param r The tree to print
     *
     * @return String A single-line ordered string representation of the tree
     */
    public static <K extends Comparable<? super K>,V> String printOrdered(TreeMap<K,V> m) {
        return printOrdered(m.getRoot());
    }


    /**
     * <p>
     * Prints an in-order traversal of the tree to the given PrintStream.
     * </p>
     *
     * @param r The tree to print
     * @param s The stream to print to (such as System.out)
     */
    public static <K extends Comparable<? super K>,V> void printOrdered(TreeMap<K,V> m, PrintStream s) {
        s.append(printOrdered(m.getRoot()));
    }


    /**
     * <p>
     * Prints an in-order traversal of the tree to the given PrintStream.
     * </p>
     *
     * @param r The root of the tree to print
     * @param s The stream to print to (such as System.out)
     */
    public static <K extends Comparable<? super K>,V> void printOrdered(TreeMapNode<K,V> r, PrintStream s) {
        s.append(printOrdered(r));
    }


    /**
     * <p>
     * Returns a string representation of a level-order traversal of the tree
     * </p>
     *
     * @param r The root of the tree to print
     *
     * @return String A single-line ordered string representation of the tree
     */
    public static <K extends Comparable<? super K>,V> String printOrdered(TreeMapNode<K,V> r) {
        StringBuilder sb = new StringBuilder(100);
        printOrdered(r, sb);
        return sb.toString();
    }


    // Private implementation and helpers below


    /**
     * <p>
     * Returns a string representation of a level-order traversal of the tree
     * </p>
     *
     * @param r The root of the tree to print
     * @param sb A StringBuilder to accumulate printed results
     */
    private static <K extends Comparable<? super K>,V> void printLevel(TreeMapNode<K,V> r, StringBuilder sb) {
        Queue<QNode<K,V>> q = new LinkedList<>();
        q.add(new QNode<K,V>(r, 0));

        // get formatting info, assume zero depth and minimum
        // key/value length (can't use zero here; strfmt error)
        int[] max = { 0, 1, 1 };
        levelMax(r, 0, max);
        String nf = "[%" + max[1] + "s/%" + max[2] + "s]"; // node format

        int level = 0;
        int maxNodes = (int)Math.pow(2, max[0]);
        int levelNodes = 1;
        while (q.size() > 0) {
            QNode<K,V> qn = q.remove();

            // check if the level has changed (new line)
            if (qn.l != level) {
                sb.append("\n\n");
                level = qn.l;
                levelNodes = levelNodes * 2;
            }

            for (int i = 0; i < (max[1] + max[2] + 4) * (maxNodes / levelNodes - 1) / 2; i++) {
                sb.append(" ");
            }

            // print out the value
            if (null == qn.n) {
                sb.append(String.format(nf, "", ""));
                if (level < max[0]) {
                    q.add(new QNode<K,V>(null,level+1)); // padding nulls
                    q.add(new QNode<K,V>(null,level+1));
                }
            }
            else {
                sb.append(String.format(nf, qn.n.getKey(), qn.n.getValue()));

                if (level < max[0]) {
                    q.add(new QNode<K,V>(qn.n.getLeft(), qn.l+1));
                    q.add(new QNode<K,V>(qn.n.getRight(), qn.l+1));
                }
            }

            for (int i = -1; i < (max[1] + max[2] + 4) * (maxNodes / levelNodes - 1) / 2; i++) {
                sb.append(" ");
            }

        }

        sb.append("\n");
    }


    /**
     * <p>
     * Recursive helper to perform an in-order traversal of the tree
     * </p>
     *
     * @param r The root of the tree to print
     * @param sb The StringBuilder to accumulate output into
     */
    private static <K extends Comparable<? super K>,V> void printOrdered(TreeMapNode<K,V> r, StringBuilder sb) {
        if (r == null) {
            return;
        }

        printOrdered(r.getLeft(), sb);

        sb.append(" [" + r.getKey() + "/" + r.getValue() + "]");

        printOrdered(r.getRight(), sb);
    }


    /**
     * <p>
     * Helper class to store a node and its level in a queue as a single
     * object.  Used by the printLevel() method.
     * </p>
     */
    private static class QNode<K extends Comparable<? super K>,V> {
        protected TreeMapNode<K,V> n;
        protected int l = 0;

        public QNode(TreeMapNode<K,V> node, int level) {
            n = node;
            l = level;
        }
    }


    /**
     * <p>
     * Helper method that determines the maximum depth of a tree,
     * maximum key length (as a string) and maximum value length
     * (as a string) for a tree rooted at r.  Used by the printLevel()
     * method to determine how much space to set aside for each level
     * when formatting the tree.
     *
     * @param r The root of the tree to search
     * @param l The current level in the tree (should be zero for initial call)
     * @param a The array to store the max depth, max key length,
     *          and max value length (respectively)
     */
    private static <K extends Comparable<? super K>,V> void levelMax(TreeMapNode<K,V> r, int l, int[] a) {
        if (null == r) {
            return;
        }

        if (l > a[0]) {
            a[0] = l;
        }

        int kl = r.getKey() != null ? r.getKey().toString().length() : 0;
        int vl = r.getValue() != null ? r.getValue().toString().length() : 0;

        if (kl > a[1]) {
            a[1] = kl;
        }
        if (vl > a[2]) {
            a[2] = vl;
        }

        levelMax(r.getLeft(), l+1, a);
        levelMax(r.getRight(), l+1, a);
    }


} // end of class TreeUtil
