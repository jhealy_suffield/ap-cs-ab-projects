package org.suffieldacademy.apcs.bst;


/**
 * <p>
 * <b>Set</b> represents a set of keys; each is associated with a value.
 * A map may not contain more than one object with the same key.
 * </p>
 *
 * @author Jason Healy
 * @version $Revision: 723 $
 *
 */
public interface Set<K> {

    /**
     * Adds the given item to the set.  If the item already exists,
     * the method returns false and the set remains unchanged.  Otherwise,
     * the item is added to the set and the method returns true.
     *
     * @param o The item to add to the set
     *
     * @return boolean True if the item doesn't already exist in the set;
     *                 false otherwise.
     */
    public boolean add(K o);


    /**
     * Removes the given item from the set.  If the item exists in the set,
     * remove it and return true.  Otherwise, the set remains unchanged
     * and the method returns false.
     *
     * @param o The item to remove from the set
     *
     * @return boolean True if the item is found and removed; false otherwise
     */
    public boolean remove(K o);


    /**
     * Returns true if the given item is found in the set, or false if it
     * is not found.
     *
     * @param o The item to search for
     *
     * @return boolean True if the item is in the set; false otherwise
     */
    public boolean contains(K o);


    /**
     * Returns the size (number of items) in this set.
     *
     * @return int The number of items in the set
     */
    public int size();


    /**
     * Copies all of the items in the set into an array.  The array
     * should have the exact same size as the set.
     *
     * @return Object[] An array holding each item in the set
     */
    public Object[] toArray();


} // end of class Set
