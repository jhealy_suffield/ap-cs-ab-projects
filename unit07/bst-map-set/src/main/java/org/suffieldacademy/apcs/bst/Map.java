package org.suffieldacademy.apcs.bst;


/**
 * <p>
 * <b>Map</b> represents a set of keys; each is associated with a value.
 * A map may not contain more than one object with the same key.
 * </p>
 *
 * @author Jason Healy
 * @version $Revision: 723 $
 *
 */
public interface Map<K,V> {


    /**
     * Adds the given key and its associated value to the map.  If the
     * given key already exists in the map, the method should return the
     * old value that was associated with the key.  Otherwise, the method
     * returns null.
     *
     * @param key The key to add to the map
     * @param value The value to associate with the key
     *
     * @return V The preexisting value associated with key if key
     *           is already in the map; otherwise null
     */
    public V put(K key, V value);


    /**
     * Gets the object associated with the given key in the map.  If
     * the key does not exist in the map, the method returns null.
     *
     * @param key The key to search for
     *
     * @return V The value associated with the key, or null if the
     *           key does not exist in the map
     */
    public V get(K key);


    /**
     * Removes the given key from the map, along with its associated value.
     * The method returns the value associated with the key.  If the
     * key does not exist in the map, the method returns null.
     *
     * @param key The key to remove from the map
     *
     * @return V The value associated with the key before removal,
     *           or null if the key does not exist in the map
     */
    public V remove(K key);


    /**
     * Searches for the given key in the map and returns true if the key
     * is found.  Returns false otherwise.
     *
     * @param key The key to search for in the map
     *
     * @return boolean True if the key exists in the map; false otherwise
     */
    public boolean containsKey(K key);


    /**
     * Returns the size (number of keys) in this map
     *
     * @return int The number of keys in this map
     */
    public int size();


    /**
     * Returns a Set containing all the keys in this map.  Because a Set
     * is just like a map (except that the associated values are omitted),
     * we can simply construct a TreeSet using this map as the storage
     * for the set.
     *
     * @return Set A Set object containing all the keys of this map
     */
    public Set<K> keySet();


} // end of class Map
