package org.suffieldacademy.apcs.bst;


/**
 * <p>
 * <b>TreeMapNode</b> is a building block to create a binary search tree
 * that can be used as the backing for a Map or Set.  Each node carries
 * four pieces of information: a link to a left and right node, a key
 * object, and a value object.
 * </p>
 *
 * @author Jason Healy
 * @internal Not student edited
 *
 */
public class TreeMapNode<K extends Comparable<? super K>,V>
  implements Comparable<TreeMapNode<K,V>> {

    /** Link to left child */
    protected TreeMapNode<K,V> left;

    /** Link to right child */
    protected TreeMapNode<K,V> right;

    /** Key object stored with this node */
    protected K key;

    /** Value object stored with this node */
    protected V value;


    /**
     * <p>
     * Create a new TreeMapNode.
     * </p>
     *
     * @param k The key for this node
     * @param v The value for this node
     * @param l The initial left child node
     * @param r The initial right child node
     */
    public TreeMapNode(K k, V v, TreeMapNode<K,V> l, TreeMapNode<K,V> r) {
        key = k;
        value = v;
        left = l;
        right = r;
    }


    /**
     * <p>
     * Create a new TreeMapNode with null children.
     * </p>
     *
     * @param k The key for this node
     * @param v The value for this node
     */
    public TreeMapNode(K k, V v) {
        this(k, v, null, null);
    }


    // Accessor and Mutator methods

    public void setKey(K k) { key = k; }
    public K getKey() { return key; }

    public void setValue(V v) { value = v; }
    public V getValue() { return value; }

    public void setLeft(TreeMapNode<K,V> n) { left = n; }
    public TreeMapNode<K,V> getLeft() { return left; }

    public void setRight(TreeMapNode<K,V> n) { right = n; }
    public TreeMapNode<K,V> getRight() { return right; }


    // Make nodes comparable (based on their key) as a shortcut
    // (so you don't have to get the keys and compare them directly)
    public int compareTo(TreeMapNode<K,V> that) {
      return this.key.compareTo(that.key);
    }


} // end of class TreeMapNode
