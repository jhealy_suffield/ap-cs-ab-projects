package org.suffieldacademy.apcs.bst;


/**
 * <p>
 * <b>TreeSet</b> implements the Set interface using a Binary Search Tree
 * to sort and store nodes.
 * </p>
 *
 * <p>
 * Rather than write the entire BST implementation from scratch, it's much
 * easier to simply use a Map as the backing store for the set (because
 * a set is exactly like a map, except that there are no values associated
 * with the keys).  Therefore, you should <b>not</b> implement a BST from
 * scratch here.
 * </p>
 *
 * @author Your Name
 */
public class TreeSet<K extends Comparable<? super K>> implements Set<K> {

    /** TreeMap instance used to back the set */
    private TreeMap<K, ?> map;


    /**
     * <p>
     * Constructor: create a new empty set.
     * </p>
     */
    public TreeSet() {
        // Use a Map as the backing store for this set (we'll just
        // ignore the values and only use the keys)
        map = new TreeMap<K,Object>();
    }


    /**
     * <p>
     * Constructor, using an existing map as the backing store.
     * </p>
     *
     * <p>
     * Precondition: startMap is not null
     * </p>
     *
     * @param startMap The map to use as the backing store.
     */
    public TreeSet(TreeMap<K,?> startMap) {
        map = startMap;
    }


    /**
     * @see Set#add(K) Set Interface Specification for add()
     */
    public boolean add(K o) {
        // FIXME (only here so the code compiles; you should return a value)
        return false;
    }


    /**
     * @see Set#remove(K) Set Interface Specification for remove()
     */
    public boolean remove(K o) {
        // FIXME (only here so the code compiles; you should return a value)
        return false;
    }


    /**
     * @see Set#contains(K) Set Interface Specification for contains()
     */
    public boolean contains(K o) {
        // FIXME (only here so the code compiles; you should return a value)
        return false;
    }


    /**
     * @see Set#size() Set Interface Specification for size()
     */
    public int size() {
        // FIXME (only here so the code compiles; you should return a value)
        return -1;
    }


    /**
     * @see Set#toArray() Set Interface Specification for toArray()
     *
     * Because the set is backed by a Binary Search Tree, the array
     * should contain the items in ascending order.
     */
    public Object[] toArray() {
        // FIXME: create an array of the correct size and fill it with
        //        all of the keys in the Set

        // hint: use an in-order traversal, and feel free to create
        // a private recursive helper method if you want...
        return null;
    }


} // end of class TreeSet
