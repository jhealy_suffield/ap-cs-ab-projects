package org.suffieldacademy.apcs.bst;

// junit dependencies
import static org.junit.Assert.*;
import org.junit.Test;

// random stress-test dependencies
import java.util.Random;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * <p>
 * <b>TreeMapTest</b> checks the implementation of TreeMap to ensure
 * that it performs all operations correctly.
 * </p>
 *
 * @author Jason Healy
 */
public class TreeMapTest {

    @Test
    public void testEmpty() {

        Map<Integer,String> map = new TreeMap<Integer,String>();

        assertEquals("A new map should have a size of zero",
                     0, map.size());

        String get = map.get(Integer.MIN_VALUE);

        assertEquals("Getting a non-existant value from a map should return null",
                     null, get);
    }


    @Test
    public void testPutGetOne() {

        Map<Integer,String> map = new TreeMap<Integer,String>();

        int k1 = 1;
        String v1 = "one";

        Object old = map.put(k1, v1);

        assertEquals("A map with one thing put in it should have a size of 1",
                     1, map.size());

        assertEquals("Put should return null when adding a completely new key",
                     null, old);

        Object g1 = map.get(k1);

        assertEquals("Calling get on a key should return what you put in",
                     v1, g1);

        String v1take2 = "one - second time";
        old = map.put(k1, v1take2);

        assertEquals("A map with two items having the same key should only have size 1",
                     1, map.size());

        assertEquals("Put should return the old value when putting a duplicate key",
                     v1, old);

        Object g1take2 = map.get(k1);

        assertEquals("Calling get on a duplicate key should return most recent value",
                     v1take2, g1take2);

    }


    @Test
    public void testPutGetMany() {
        Map<Integer,String> map = new TreeMap<Integer,String>();

        int[] keys = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        String[] values = {
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten"
        };

        int[] ordering = { 5, 2, 3, 4, 1, 0, 8, 9, 6, 7 };

        for (int i = 0; i < ordering.length; i++) {
            Object old = map.put(keys[ordering[i]], values[ordering[i]]);
            assertEquals("New key should return null on put", null, old);
        }

        assertEquals("Size of map does not match number of items put in",
                     keys.length, map.size());

        for (int i = 0; i < keys.length; i++) {
            Object val = map.get(keys[i]);
            assertEquals("Retrieved value does not equal put value for " + keys[i],
                         values[i], val);

        }

        // insert values with mixed up keys, and confirm that new values stick
        for (int i = 0; i < keys.length; i++) {
            Object old = map.put(keys[i], values[values.length-1-i]);
            assertEquals("Duplicate key put should return old value",
                         values[i], old);
        }

        assertEquals("Size of map should not change when adding duplicate keys",
                     keys.length, map.size());

        for (int i = 0; i < keys.length; i++) {
            Object val = map.get(keys[i]);
            assertEquals("Retrieved value does not equal new put value",
                         values[values.length-1-i], val);
        }
    }


    @Test
    public void testContainsOne() {

        Map<Integer,String> map = new TreeMap<Integer,String>();

        int k1 = 1;
        String v1 = "one";

        Object old = map.put(k1, v1);

        assertEquals("Map should contain recently added key",
                     true, map.containsKey(k1));

    }


    @Test
    public void testContainsNull() {

        Map<Integer,String> map = new TreeMap<Integer,String>();

        int k1 = 0;
        String v1 = null;

        Object old = map.put(k1, v1);

        assertEquals("Map should contain key with associated null value",
                     true, map.containsKey(k1));

    }


    @Test
    public void testRemoveOne() {

        Map<Integer,String> map = new TreeMap<Integer,String>();

        int k1 = 1;
        String v1 = "one";

        map.put(k1, v1);

        Object dead = map.remove(k1);

        assertEquals("Remove should return removed value",
                     v1, dead);

        assertEquals("Size should decrement when a key is removed",
                     0, map.size());

        Object zombie = map.get(k1);

        assertEquals("Should not be able to get removed key",
                     null, zombie);

        assertEquals("Map should not contain a removed value",
                     false, map.containsKey(k1));

    }


    @Test
    public void testRemoveNullKey() {

        Map<Integer,String> map = new TreeMap<Integer,String>();

        int k1 = 0;
        String v1 = null;

        map.put(k1, v1);

        Object dead = map.remove(k1);

        assertEquals("Remove should return removed null value",
                     v1, dead);

        assertEquals("Size should decrement when a key with null value is removed",
                     0, map.size());

        assertEquals("Map should not contain a removed null value",
                     false, map.containsKey(k1));

    }


    @Test
    public void testRemoveMany() {
        Map<Integer,String> map = new TreeMap<Integer,String>();

        int[] keys = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        String[] values = {
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten"
        };

        int[] ordering = { 5, 2, 3, 4, 1, 0, 8, 9, 6, 7 };

        for (int i = 0; i < ordering.length; i++) {
            Object old = map.put(keys[ordering[i]], values[ordering[i]]);
        }

        for (int i = 0; i < keys.length; i++) {
            Object dead = map.remove(keys[i]);
            assertEquals("Removed value does not equal put value for " + keys[i],
                         values[i], dead);

            assertEquals("Size did not decrement after removing key",
                         keys.length - 1 - i, map.size());

        }

        // double-check removed items to make sure they're gone
        for (int i = 0; i < keys.length; i++) {
            Object dead = map.get(keys[i]);
            assertEquals("Should not be able to retrieve a deleted key",
                         null, dead);

            assertEquals("Map should not contain a deleted key",
                         false, map.containsKey(keys[i]));
        }

    }


    @Test
    public void testRemoveSpecifics() {
        TreeMap<Integer,String> map = new TreeMap<Integer,String>();

        int[] keys = { 0,1,2,3,4,5,6,7,8,9,10,11,12 };

        String[] values = {
            "zero",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten",
            "eleven",
            "twelve"
        };

        int[] ordering = { 8, 4, 10, 2, 6, 9, 11, 1, 3, 5, 7, 12, 0 };

        for (int i = 0; i < ordering.length; i++) {
            String old = map.put(keys[ordering[i]], values[ordering[i]]);
        }

        String dead = null;
        String zombie = null;

        int[] rmOrder = {
            12, // no children
            1,  // only left child
            11, // only right child
            4,  // two children
            8   // root, two children, multiple descendants
        };
        String[] rmDesc = {
            "no children",
            "only left child",
            "only right child",
            "two children, multiple descendants",
            "root, two children, multiple descendants"
        };
        // keep a list of all nodes so we can ensure some aren't
        // accidentally deleted
        int[] remaining = { 12, 1, 11, 4, 8, 10, 2, 6, 9, 3, 5, 7, 0 };

        for (int i=0; i<rmOrder.length; i++) {
            String before = TreeUtil.printLevel(map);
            dead = map.remove(keys[rmOrder[i]]);
            String after = TreeUtil.printLevel(map);
            String compare = "Tree before removal of key '"
                + keys[rmOrder[i]] + "':\n" + before
                + "\nTree after removal of key '"
                + keys[rmOrder[i]] + "':\n" + after + "\n";
            assertEquals("Removed value does not equal put value for '"
                         + rmDesc[i] + "' test key '" + keys[rmOrder[i]]
                         + "';\n" + compare,
                         values[rmOrder[i]], dead);
            zombie = map.get(keys[rmOrder[i]]);
            assertEquals("Should not be able to retrieve deleted key '"
                         + keys[rmOrder[i]] + "' after test '"
                         + rmDesc[i] + "';\n" + compare,
                         null, zombie);
            for (int r=i+1; r < remaining.length; r++) {
                String alive = map.get(remaining[r]);
                assertEquals("Key '" + remaining[r]
                             + "' was inadvertantly modified as a "
                             + "side-effect of removing key '"
                             + keys[rmOrder[i]] + "';\n" + compare,
                             values[remaining[r]], alive);
            }
        }
    }


    @Test
    public void testRandomStress() {
        Map<String, String> map = new TreeMap<String, String>();

        Random r = new Random();

        LinkedList<String> keys = new LinkedList<String>();
        LinkedList<String> values = new LinkedList<String>();

        HashSet<Integer> taken = new HashSet<Integer>();

        int x = r.nextInt();

        ListIterator<String> ki = null;
        ListIterator<String> vi = null;
        int size = 0;

        for (int passes = 0; passes < 100; passes++) {

            int maxAdd = r.nextInt(1000);
            //System.out.println("Random Pass " + passes + ": Adding " + maxAdd + " new items");
            for (int i = 0; i < maxAdd; i++) {
                // ensure key is unique
                while (taken.contains(x)) {
                    x = r.nextInt();
                }
                taken.add(x);
                keys.addLast(""+x+"k");
                values.addLast(""+x+"v");
                String old = map.put(keys.getLast(), values.getLast());
                assertEquals("New key should return null on put", null, old);
            }

            assertEquals("Size of map does not match number of items put in",
                         keys.size(), map.size());

            // check add
            ki = keys.listIterator();
            vi = values.listIterator();
            while (ki.hasNext()) {
                String key = ki.next();
                String val = vi.next();

                String got = map.get(key);
                assertEquals("Retrieved value does not equal put value for " + key,
                             val, got);
            }

            // check remove
            ki = keys.listIterator();
            vi = values.listIterator();
            size = keys.size();
            int maxRemove = r.nextInt(keys.size());
            //System.out.println("Random Pass " + passes + ": Removing " + maxRemove + " items");
            while (ki.hasNext() && maxRemove > 0) {
                maxRemove--;
                String key = ki.next();
                String val = vi.next();

                String dead = map.remove(key);
                size--;
                assertEquals("Removed value does not equal put value for " + key,
                             val, dead);

                assertEquals("Size did not decrement after removing key",
                             size, map.size());

                // double-check removed items to make sure they're gone
                ki.remove();
                vi.remove();

                String zombie = map.get(key);
                assertEquals("Should not be able to retrieve a deleted key",
                             null, zombie);

                assertEquals("Map should not contain a deleted key",
                             false, map.containsKey(key));

            }

        } // end of passes

        // now try to remove any remaining items and zero out the hash
        ki = keys.listIterator();
        vi = values.listIterator();
        size = keys.size();
        //System.out.println("Final Pass: Removing all " + size + " remaining items");
        while (ki.hasNext()) {
            String key = ki.next();
            String val = vi.next();

            String dead = map.remove(key);
            size--;
            assertEquals("Removed value does not equal put value for " + key,
                             val, dead);

            assertEquals("Size did not decrement after removing key",
                         size, map.size());

            // double-check removed items to make sure they're gone

            ki.remove();
            vi.remove();

            String zombie = map.get(key);
            assertEquals("Should not be able to retrieve a deleted key",
                         null, zombie);

            assertEquals("Map should not contain a deleted key",
                         false, map.containsKey(key));
        }

        assertEquals("Map should be empty (size 0) after random test",
                     0, map.size());

    }


} // end of class TreeMapTest
