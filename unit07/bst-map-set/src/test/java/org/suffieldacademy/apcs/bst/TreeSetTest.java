package org.suffieldacademy.apcs.bst;

// junit dependencies
import static org.junit.Assert.*;
import org.junit.Test;


/**
 * <p>
 * <b>TreeSetTest</b> checks the implementation of TreeSet to ensure
 * that it performs all operations correctly.
 * </p>
 *
 * @author Jason Healy
 */
public class TreeSetTest {

    @Test
    public void testEmpty() {

        Set<Integer> set = new TreeSet<Integer>();

        assertEquals("A new set should have a size of zero",
                     0, set.size());

        assertEquals("Set should not contain non-existant member",
                     false, set.contains(Integer.MIN_VALUE));
    }


    @Test
    public void testAddOne() {

        Set<Integer> set = new TreeSet<Integer>();

        int k1 = 1;

        assertEquals("Add should return true when adding a completely new item",
                     true, set.add(k1));

        assertEquals("A set with one thing put in it should have a size of 1",
                     1, set.size());

        assertEquals("A set should contain something that was just added",
                     true, set.contains(k1));

        int k2 = 1;

        assertEquals("Add should return false when adding a duplicate item",
                     false, set.add(k2));

        assertEquals("A set with duplicate items shouldn't change size",
                     1, set.size());

        assertEquals("A set should still contain something after a duplicate add",
                     true, set.contains(k2));

    }


    @Test
    public void testPutGetMany() {
        Set<Integer> set = new TreeSet<Integer>();

        int[] keys = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        int[] ordering = { 5, 2, 3, 4, 1, 0, 8, 9, 6, 7 };

        for (int i = 0; i < ordering.length; i++) {
            assertEquals("New key should return true on add", true,
                         set.add(keys[ordering[i]]));
        }

        assertEquals("Size of set does not match number of items put in",
                     keys.length, set.size());

        for (int i = 0; i < keys.length; i++) {
            assertEquals("Set does not contain " + keys[i],
                         true, set.contains(keys[i]));

        }

        // insert values with mixed up keys
        for (int i = 0; i < keys.length; i++) {
            assertEquals("Duplicate add should return false",
                         false, set.add(keys[i]));
        }

        assertEquals("Size of set should not change when adding duplicate keys",
                     keys.length, set.size());

    }


    @Test
    public void testRemoveOne() {

        Set<Integer> set = new TreeSet<Integer>();

        int k1 = 1;

        set.add(k1);

        assertEquals("Remove should return true for existing value",
                     true, set.remove(k1));

        assertEquals("Size should decrement when a key is removed",
                     0, set.size());

        assertEquals("Set should not contain removed key",
                     false, set.contains(k1));

        assertEquals("Remove should return false for non-existant value",
                     false, set.remove(100));

    }


    @Test
    public void testRemoveMany() {
        Set<Integer> set = new TreeSet<Integer>();

        int[] keys = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        int[] ordering = { 5, 2, 3, 4, 1, 0, 8, 9, 6, 7 };

        for (int i = 0; i < ordering.length; i++) {
            set.add(keys[ordering[i]]);
        }

        for (int i = 0; i < keys.length; i++) {
            assertEquals("Remove should return true for exiting items",
                         true, set.remove(keys[i]));

            assertEquals("Size did not decrement after removing key",
                         keys.length - 1 - i, set.size());

        }

        // double-check removed items to make sure they're gone
        for (int i = 0; i < keys.length; i++) {
            assertEquals("Set should not contain a deleted item",
                         false, set.contains(keys[i]));
        }

    }


    @Test
    public void testToArray() {
        Set<Integer> set = new TreeSet<Integer>();

        int[] keys = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        int[] ordering = { 5, 2, 3, 4, 1, 0, 8, 9, 6, 7 };

        for (int i = 0; i < ordering.length; i++) {
            set.add(keys[ordering[i]]);
        }

        Object[] a = set.toArray();

        assertEquals("toArray() should return an array the same size as the set",
                     set.size(), a.length);

        assertEquals("toArray() should return an array the same size as what we put in",
                     keys.length, a.length);

        for (int i = 0; i < a.length; i++) {
            assertEquals("Item in returned array at position " + i +
                         " is wrong or out of order",
                         keys[i], ((Integer)a[i]).intValue());
        }

    }


} // end of class TreeSetTest
