// $Id: QueueTest.java 732 2020-01-30 14:34:50Z jhealy $
package org.suffieldacademy.apcs.queue;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;


/**
 * <p>
 * <b>QueueTest</b> checks the implementation of Queue to ensure
 * that it performs all operations correctly.
 * </p>
 *
 * <p>This class is abstract, but defines all of the test methods.
 * Concrete classes select the implementation (List or Array) to
 * test, and rely on the inherited methods to do the work.
 *
 * @author Jason Healy
 */
public abstract class QueueTest {

    /** Queue interface variable, to be initialized by subclass. */
    protected Queue<String> q = null;

    /** Subclass should use this method to initialize the variable above. */
    @Before
    public abstract void beforeEachTest();

    /** Set the variable back to null after each test. */
    @After
    public void afterEachTest() {
        q = null;
    }

    public void dqEmpty() {
        q.dequeue();
    }


    public void peekEmpty() {
        q.peekFront();
    }


    public void isEmpty() {
        assertTrue("Empty queue should be empty", q.isEmpty());
        q.enqueue("A");
        assertFalse("Queue after enqueue should not be empty", q.isEmpty());
    }


    public void basicNqDq() {

        String a = "A";
        q.enqueue(a);
        assertEquals("Just-enqueued item should be returned by peekFront()",
                     a, q.peekFront());
        assertEquals("Just-enqueued item should be returned by dequeue()",
                     a, q.dequeue());
    }


    public void orderTest() {

        int capacity = 0;

        try {
            for (int i = 1; i <= 100; i++) {
                q.enqueue(""+i);
                capacity++;
            }
        }
        catch (IllegalStateException ise) {
            // queue must be full, so that's as far as we can go
        }

        for (int i = 1; i <= capacity; i++) {
            assertEquals("Dequeued item not correct",
                         ""+i, q.dequeue());
        }
    }


    public void nqDqEmpty() {
        q.enqueue("A");
        q.dequeue();
        assertTrue("Queue should be empty after enqueue/dequeue combo", q.isEmpty());
    }


    public void randomInterleave() {
        final int maxRuns = 10000;

        java.util.Queue<String> j = new java.util.LinkedList<String>();

        java.util.Random r = new java.util.Random();

        int i = 1;
        int run = 0;
        while (run < maxRuns || j.size() > 0) {
            if (run < maxRuns && r.nextBoolean()) { // enqueue
                try {
                    //System.out.println("nq " +  i);
                    q.enqueue(""+i);
                    j.offer(""+i);
                    i++;
                }
                catch (IllegalStateException ise) {
                    // queue must be full, so just carry on
                }
            }
            else { // dequeue
                try {
                    String sq = q.dequeue();
                    String sj = j.poll();
                    //System.out.println("dq " + sj);

                    assertEquals("dequeued item not correct",
                     sj, sq);
                }
                catch (RuntimeException re) {
                    // queue must be empty, so just carry on
                }
            }
            run++;
        }
    }


} // end of class QueueTest
