// $Id: LinkedQueueTest.java 732 2020-01-30 14:34:50Z jhealy $
package org.suffieldacademy.apcs.queue;

import org.junit.Test;
import java.util.NoSuchElementException;


/**
 * <p>
 * Subclass of QueueTest that specifies all tests to be run on an LinkedQueue.
 * </p>
 *
 * @author Jason Healy
 */
public class LinkedQueueTest extends QueueTest {

    @Override
    public void beforeEachTest() {
        q = new LinkedQueue<String>();
    }

    // we "repeat" the inherited methods here so the test runner
    // can discover them for individual running

    @Test(expected = NoSuchElementException.class)
    public void dqEmpty() {
        super.dqEmpty();
    }

    @Test(expected = NoSuchElementException.class)
    public void peekEmpty() {
        super.peekEmpty();
    }

    @Test
    public void isEmpty() {
        super.isEmpty();
    }

    @Test
    public void basicNqDq() {
        super.basicNqDq();
    }

    @Test
    public void orderTest() {
        super.orderTest();
    }

    @Test
    public void nqDqEmpty() {
        super.nqDqEmpty();
    }

    @Test
    public void randomInterleave() {
        super.randomInterleave();
    }

} // end of class LinkedQueueTest
