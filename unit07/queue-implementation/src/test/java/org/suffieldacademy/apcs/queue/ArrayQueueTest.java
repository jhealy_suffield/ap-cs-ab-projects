// $Id: ArrayQueueTest.java 732 2020-01-30 14:34:50Z jhealy $
package org.suffieldacademy.apcs.queue;

import static org.junit.Assert.*;
import org.junit.Test;
import java.util.NoSuchElementException;


/**
 * <p>
 * Subclass of QueueTest that specifies all tests to be run on an ArrayQueue.
 * </p>
 *
 * @author Jason Healy
 */
public class ArrayQueueTest extends QueueTest {

    @Override
    public void beforeEachTest() {
        q = new ArrayQueue<String>();
    }

    // special test only for fixed-size array queue
    @Test(expected=IllegalStateException.class)
    public void full() {
        for (int i = Integer.MIN_VALUE; i < Integer.MAX_VALUE; i++) {
            q.enqueue(""+i);
        }
    }

    // we "repeat" the inherited methods here so the test runner
    // can discover them for individual running

    @Test(expected = NoSuchElementException.class)
    public void dqEmpty() {
        super.dqEmpty();
    }

    @Test(expected = NoSuchElementException.class)
    public void peekEmpty() {
        super.peekEmpty();
    }

    @Test
    public void isEmpty() {
        super.isEmpty();
    }

    @Test
    public void basicNqDq() {
        super.basicNqDq();
    }

    @Test
    public void orderTest() {
        super.orderTest();
    }

    @Test
    public void nqDqEmpty() {
        super.nqDqEmpty();
    }

    @Test
    public void capacity() {
        // Should be able to create a queue with a specific capacity
        // and fill it full (no unused space)

        int size = 100;

        q = new ArrayQueue<String>(size);

        for (int i = 1; i <= size; i++) {
            try {
                q.enqueue(""+i+"/"+size);
            }
            catch (Exception e) {
                fail("Queue created with fixed capapcity was not able to " +
                     "enqueue all elements; failed on item " + i +
                     " out of " + size + " (capacity appears to be " +
                     (size-i+1) + " item(s) too small) with " + e);
            }
        }

        for (int i = 1; i <= size; i++) {
            try {
                assertEquals("Wrong element dequeued",
                             (""+i+"/"+size), q.dequeue());
            }
            catch (Exception e) {
                fail("Queue created with fixed capapcity was not able to " +
                     "dequeue all elements (even though enqueue worked?!?); " +
                     "failed on item " + i + " out of " + size +
                     " (capacity appears to be " + (size-i+1) +
                     " item(s) too small) with " + e);
            }
        }

    }

    @Test
    public void wrap() {
        // Should be able to add and remove a single item over and over
        // without exceeding the size of the array.  This should cause
        // the items to travel through the queue over time to wrap around.

        q.enqueue("A");

        for (int i = 0; i < 5000; i++) {
            q.enqueue("B");
            assertEquals("Wrap test A element wrong", "A", q.dequeue());
            q.enqueue("A");
            assertEquals("Wrap test B element wrong", "B", q.dequeue());
        }

        assertEquals("Final dequeue wrong", "A", q.dequeue());
    }

    @Test
    public void randomInterleave() {
        super.randomInterleave();
    }

} // end of class ArrayQueueTest
