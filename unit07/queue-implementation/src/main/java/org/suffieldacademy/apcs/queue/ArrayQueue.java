package org.suffieldacademy.apcs.queue;

import java.util.NoSuchElementException;


/**
 * <p>
 * <b>ArrayQueue</b> implements the Queue interface using an
 * array-backed data structure.  This class must implement a Queue
 * using a FIXED-SIZE array using a CIRCULAR (ring buffer) strategy.
 * All enqueue/dequeue operations must be constant O(1) time operations.
 * Because the queue has a fixed size, you may throw an exception when
 * trying to enqueue an item to a full buffer, or overwrite old data
 * (please document which strategy you're using in the comments).
 * </p>
 *
 * @author Your Name
 *
 */
public class ArrayQueue<E> implements Queue<E> {

    /** Array to store the queue data */
    private E[] a;

    // your variables here (you need to keep track of the head and
    // tail of the queue)


    /**
     * Constructor: create an empty queue with the given capacity.
     */
    public ArrayQueue(int capacity) {
        // YOUR CODE HERE: initialize the array "a" with a properly-sized
        // array, and initialize your variables
    }

    /**
     * Constructor: create an empty queue with a default size.
     */
    public ArrayQueue() {
        // you do not need to modify this constructor; only the non-default
        // one above

        // use the primary constructor and provide a default capacity
        this(100);
    }


    /**
     * postcondition: returns true if queue is empty;
     *                otherwise, returns false
     */
    public boolean isEmpty() {
        return true; // fix and replace with your code
    }


    /**
     * precondition:  queue is [e1, e2, ..., en] with n >= 0
     * postcondition: queue is [e1, e2, ..., en, x]
     *
     * @throws IllegalStateException if the queue is full
     */
    public void enqueue(E x) {
        // add your code here

        // you may do this if the queue is full and cannot accept new items
        throw new IllegalStateException("Queue is full");
    }


    /**
     * precondition:  queue is [e1, e2, ..., en] with n >= 1
     * postcondition: queue is [e2, ..., en]; returns e1
     * exceptions: throws a NoSuchElementException if the queue is empty
    */
    public E dequeue() {
        return null; // fix and replace with your code
    }


    /**
     * precondition:  queue is [e1, e2, ..., en] with n >= 1
     * postcondition: returns e1; queue is unchanged
     * exceptions: throws a NoSuchElementException if the queue is empty
     */
    public E peekFront() {
        return null; // fix and replace with your code
    }


} // end of class ArrayQueue
