package org.suffieldacademy.apcs.queue;


/**
 * <p>
 * <b>Queue</b> interface from the AP subset.  Based on a simplified
 * version of java.util.Queue.
 * </p>
 *
 * Last Modified $Date: 2020-01-30 09:34:50 -0500 (Thu, 30 Jan 2020) $ by $Author: jhealy $
 *
 */
public interface Queue<E> {

    /**
     * postcondition: returns true if queue is empty;
     *                otherwise, returns false
     */
    public boolean isEmpty();

    /**
     * precondition:  queue is [e1, e2, ..., en] with n >= 0
     * postcondition: queue is [e1, e2, ..., en, x]
     * exceptions: IllegalStateException if the queue is full
     *             (only for implementations with a fixed capacity)
     */
    public void enqueue(E x);

    /**
     * precondition:  queue is [e1, e2, ..., en] with n >= 1
     * postcondition: queue is [e2, ..., en]; returns e1
     * exceptions: throws a NoSuchElementException if the queue is empty
    */
    public E dequeue();

    /**
     * precondition:  queue is [e1, e2, ..., en] with n >= 1
     * postcondition: returns e1; queue is unchanged
     * exceptions: throws a NoSuchElementException if the queue is empty
     */
    public E peekFront();

} // end of interface Queue
