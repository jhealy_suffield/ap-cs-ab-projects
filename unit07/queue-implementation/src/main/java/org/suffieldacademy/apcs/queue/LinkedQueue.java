package org.suffieldacademy.apcs.queue;

import java.util.NoSuchElementException;


/**
 * <p>
 * <b>LinkedQueue</b> implements the Queue interface using a dynamic
 * linked data structure.  This class must implement a Queue using
 * a SINGLY-LINKED list with UNBOUNDED size.  All enqueue/dequeue
 * operations must be constant O(1) time operations.
 * </p>
 *
 * @author Your Name
 *
 */
public class LinkedQueue<E> implements Queue<E> {

    // Your private variable(s) and constructor(s) here

    // You'll either need a head and tail pointer, or a
    // circularly-linked list and a single tail pointer.


    /**
     * postcondition: returns true if queue is empty;
     *                otherwise, returns false
     */
    public boolean isEmpty() {
        return true; // fix and replace with your code
    }


    /**
     * precondition:  queue is [e1, e2, ..., en] with n >= 0
     * postcondition: queue is [e1, e2, ..., en, x]
     */
    public void enqueue(E x) {
        // fix and replace with your code
    }


    /**
     * precondition:  queue is [e1, e2, ..., en] with n >= 1
     * postcondition: queue is [e2, ..., en]; returns e1
     * exceptions: throws a NoSuchElementException if the queue is empty
    */
    public E dequeue() {
        return null; // fix and replace with your code
    }


    /**
     * precondition:  queue is [e1, e2, ..., en] with n >= 1
     * postcondition: returns e1; queue is unchanged
     * exceptions: throws a NoSuchElementException if the queue is empty
     */
    public E peekFront() {
        return null; // fix and replace with your code
    }


    /**
     *
     * <p>
     * <code>Node</code> is a private inner class used by the linked
     * list to store its data and the list structure.  Because it is
     * declared inside the LinkedQueue class, you can directly access
     * the private instance variables of the class.
     * </p>
     *
     * <p>
     * Because a queue is always accessed in order (add to tail, remove
     * from head), a singly-linked list is sufficient (you do not need
     * a "previous" link).
     * </p>
     *
     * @author Jason Healy
     *
     * @param <E> The type of data held by the node; must match the
     * enclosing list
     */
    private class Node<E> {

        private E data;
        private Node<E> next;

        /**
         * <p>
         * Creates a new list node holding the given data, and referring to
         * the given next nodes in the list.
         * </p>
         *
         * @param data The object to store in this node
         * @param next The next node in the list
         */
        public Node(E data, Node<E> next) {
            this.data = data;
            this.next = next;
        }

        // No methods are necessary; you may access the private fields
        // directly by using "node.data" syntax.

    } // end of class Node


} // end of class LinkedQueue
