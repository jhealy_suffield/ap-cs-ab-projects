package org.suffieldacademy.apcs.hash;

// junit dependencies
import static org.junit.Assert.*;
import org.junit.Test;

// random stress-test dependencies
import java.util.Random;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.ListIterator;


/**
 * <p>
 * <b>HashMapTest</b> checks the implementation of HashMap to ensure
 * that it performs all operations correctly.
 * </p>
 *
 * @author Jason Healy
 */
public class HashMapTest {

    @Test
    public void testEmpty() {

        Map<String,String> map = new HashMap<String,String>();

        assertEquals("A new map should have a size of zero",
                     0, map.size());

        Object get = map.get("Totally Bogus Key");

        assertEquals("Getting a non-existant value from a map should return null",
                     null, get);
    }


    @Test
    public void testNegativeHash() {

        Map<Integer, String> map = new HashMap<Integer,String>();

        int key = -1337;

        try {
            map.put(key, "Negative Hash Value");
            String get = map.get(key);
        }
        catch (ArrayIndexOutOfBoundsException oob) {
            fail("Key '" + key + "' has a hash code of '" + Integer.hashCode(key) +
                 "', which is negative.  When you wrap this value, are you taking care of negative values?  Be aware that mod (%) in Java returns a NEGATIVE remainder when passed a negative value!");
        }


    }


    @Test
    public void testPutGetOne() {

        Map<Integer,String> map = new HashMap<Integer,String>();

        int k1 = 1;
        String v1 = "one";

        String old = map.put(k1, v1);

        assertEquals("A map with one thing put in it should have a size of 1",
                     1, map.size());

        assertEquals("Put should return null when adding a completely new key",
                     null, old);

        String g1 = map.get(k1);

        assertEquals("Calling get on a key should return what you put in",
                     v1, g1);

        String v1take2 = "one - second time";
        old = map.put(k1, v1take2);

        assertEquals("A map with two items having the same key should only have size 1",
                     1, map.size());

        assertEquals("Put should return the old value when putting a duplicate key",
                     v1, old);

        String g1take2 = map.get(k1);

        assertEquals("Calling get on a duplicate key should return most recent value",
                     v1take2, g1take2);

    }


    @Test
    public void testPutGetMany() {
        Map<Integer, String> map = new HashMap<Integer, String>();

        int[] keys = {1,2,3,4,5,6,7,8,9,10};

        String[] values = {
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten"
        };

        for (int i = 0; i < keys.length; i++) {
            String old = map.put(keys[i], values[i]);
            assertEquals("New key should return null on put", null, old);
        }

        assertEquals("Size of map does not match number of items put in",
                     keys.length, map.size());

        for (int i = 0; i < keys.length; i++) {
            String val = map.get(keys[i]);
            assertEquals("Retrieved value does not equal put value for " + keys[i],
                         values[i], val);

        }

        // insert values with mixed up keys, and confirm that new values stick
        for (int i = 0; i < keys.length; i++) {
            String old = map.put(keys[i], values[values.length-1-i]);
            assertEquals("Duplicate key put should return old value",
                         values[i], old);
        }

        assertEquals("Size of map should not change when adding duplicate keys",
                     keys.length, map.size());

        for (int i = 0; i < keys.length; i++) {
            String val = map.get(keys[i]);
            assertEquals("Retrieved value does not equal new put value",
                         values[values.length-1-i], val);
        }
    }


    @Test
    public void testContainsOne() {

        Map<Integer,String> map = new HashMap<Integer,String>();

        Integer k1 = 1;
        String v1 = "one";

        String old = map.put(k1, v1);

        assertEquals("Map should contain recently added key",
                     true, map.containsKey(k1));

    }


    @Test
    public void testContainsNull() {

        Map<Integer,String> map = new HashMap<Integer,String>();

        Integer k1 = 0;
        String v1 = null;

        String old = map.put(k1, v1);

        assertEquals("Map should contain key with associated null value",
                     true, map.containsKey(k1));

    }


    @Test
    public void testRemoveOne() {

        Map<Integer,String> map = new HashMap<Integer,String>();

        Integer k1 = 1;
        String v1 = "one";

        map.put(k1, v1);

        String dead = map.remove(k1);

        assertEquals("Remove should return removed value",
                     v1, dead);

        assertEquals("Size should decrement when a key is removed",
                     0, map.size());

        String zombie = map.get(k1);

        assertEquals("Should not be able to get removed value",
                     null, zombie);

        assertEquals("Map should not contain a removed key",
                     false, map.containsKey(k1));

    }


    @Test
    public void testRemoveMany() {
        Map<Integer,String> map = new HashMap<Integer,String>();

        int[] keys = {1,2,3,4,5,6,7,8,9,10};

        String[] values = {
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten"
        };

        for (int i = 0; i < keys.length; i++) {
            String old = map.put(keys[i], values[i]);
        }

        for (int i = 0; i < keys.length; i++) {
            String dead = map.remove(keys[i]);
            assertEquals("Removed value does not equal put value for " + keys[i],
                         values[i], dead);

            assertEquals("Size did not decrement after removing key",
                         keys.length - 1 - i, map.size());

        }

        // double-check removed items to make sure they're gone
        for (int i = 0; i < keys.length; i++) {
            String dead = map.get(keys[i]);
            assertEquals("Should not be able to retrieve a deleted key",
                         null, dead);

            assertEquals("Map should not contain a deleted key",
                         false, map.containsKey(keys[i]));
        }

    }


    @Test
    public void testRandomStress() {
        Map<String, String> map = new HashMap<String, String>();

        Random r = new Random();

        LinkedList<String> keys = new LinkedList<String>();
        LinkedList<String> values = new LinkedList<String>();

        HashSet<Integer> taken = new HashSet<Integer>();

        int x = r.nextInt();

        ListIterator<String> ki = null;
        ListIterator<String> vi = null;
        int size = 0;

        for (int passes = 0; passes < 100; passes++) {

            int maxAdd = r.nextInt(1000);
            //System.out.println("Random Pass " + passes + ": Adding " + maxAdd + " new items");
            for (int i = 0; i < maxAdd; i++) {
                // ensure key is unique
                while (taken.contains(x)) {
                    x = r.nextInt();
                }
                taken.add(x);
                keys.addLast(""+x+"k");
                values.addLast(""+x+"v");
                String old = map.put(keys.getLast(), values.getLast());
                assertEquals("New key should return null on put", null, old);
            }

            assertEquals("Size of map does not match number of items put in",
                         keys.size(), map.size());

            // check add
            ki = keys.listIterator();
            vi = values.listIterator();
            while (ki.hasNext()) {
                String key = ki.next();
                String val = vi.next();

                String got = map.get(key);
                assertEquals("Retrieved value does not equal put value for " + key,
                             val, got);
            }

            // check remove
            ki = keys.listIterator();
            vi = values.listIterator();
            size = keys.size();
            int maxRemove = r.nextInt(keys.size());
            //System.out.println("Random Pass " + passes + ": Removing " + maxRemove + " items");
            while (ki.hasNext() && maxRemove > 0) {
                maxRemove--;
                String key = ki.next();
                String val = vi.next();

                String dead = map.remove(key);
                size--;
                assertEquals("Removed value does not equal put value for " + key,
                             val, dead);

                assertEquals("Size did not decrement after removing key",
                             size, map.size());

                // double-check removed items to make sure they're gone
                ki.remove();
                vi.remove();

                String zombie = map.get(key);
                assertEquals("Should not be able to retrieve a deleted key",
                             null, zombie);

                assertEquals("Map should not contain a deleted key",
                             false, map.containsKey(key));

            }

        } // end of passes

        // now try to remove any remaining items and zero out the hash
        ki = keys.listIterator();
        vi = values.listIterator();
        size = keys.size();
        //System.out.println("Final Pass: Removing all " + size + " remaining items");
        while (ki.hasNext()) {
            String key = ki.next();
            String val = vi.next();

            String dead = map.remove(key);
            size--;
            assertEquals("Removed value does not equal put value for " + key,
                             val, dead);

            assertEquals("Size did not decrement after removing key",
                         size, map.size());

            // double-check removed items to make sure they're gone

            ki.remove();
            vi.remove();

            String zombie = map.get(key);
            assertEquals("Should not be able to retrieve a deleted key",
                         null, zombie);

            assertEquals("Map should not contain a deleted key",
                         false, map.containsKey(key));
        }

        assertEquals("Map should be empty (size 0) after random test",
                     0, map.size());

    }


} // end of class HashMapTest
