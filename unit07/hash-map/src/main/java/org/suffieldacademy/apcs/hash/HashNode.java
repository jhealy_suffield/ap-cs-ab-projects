package org.suffieldacademy.apcs.hash;


/**
 * <p>
 * <b>HashNode</b> is a simple singly-linked list node used in conjunction
 * with a hashed array to build a HashMap.  It is used to "chain"
 * colliding elements together at the same position in an array.  This class
 * is only needed if you choose to use chaining as your method of collision
 * resolution (other methods, such as probing, do not require a linked
 * structure).
 * </p>
 *
 * @author Jason Healy
 * @internal Not student edited
 *
 */
public class HashNode<K,V> {

    /** Link to the next node in the chain */
    protected HashNode<K,V> next;

    /** Key object stored with this node */
    protected K key;

    /** Value object stored with this node */
    protected V value;


    /**
     * <p>
     * Create a new HashNode.
     * </p>
     *
     * @param k The key for this node
     * @param v The value for this node
     * @param n The initial next node
     */
    public HashNode(K k, V v, HashNode<K,V> n) {
        key = k;
        value = v;
        next = n;
    }


    /**
     * <p>
     * Create a new HashNode with null next node.
     * </p>
     *
     * @param k The key for this node
     * @param v The value for this node
     */
    public HashNode(K k, V v) {
        this(k, v, null);
    }


    // Accessor and Mutator methods

    public void setKey(K k) { key = k; }
    public K getKey() { return key; }

    public void setValue(V v) { value = v; }
    public V getValue() { return value; }

    public void setNext(HashNode<K,V> n) { next = n; }
    public HashNode<K,V> getNext() { return next; }


    // Convenience method to test if two nodes' keys are equal
    // (if the keys are equal, we say the nodes are equal as well)
    public boolean equals(Object o) {
        if (o instanceof HashNode) {
            return this.key.equals(((HashNode)o).key);
        }
        // Not a node, so it can't be equal
        return false;
    }


    public int hashCode() {
        return key.hashCode();
    }

} // end of class HashNode
