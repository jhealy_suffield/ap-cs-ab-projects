package org.suffieldacademy.apcs.hash;


/**
 * <p>
 * <b>HashMap</b> implements the Map interface using a hashed array.
 * </p>
 *
 * <p>
 * A Map is a data structure that stores pairs of objects (a "key" and
 * its associated "value").  They keys must be unique; there cannot be
 * duplicates.  The data structure allows for insertion, removal, and
 * querying for particular keys (and thus their associated values).
 * </p>
 *
 * <p>
 * You must implement an automatically-resizing HashMap using one of the
 * three collision management schemes we've learned about: bucket hashing,
 * non-linear probing, or chaining.  If you decide to use chaining, you
 * must use the included HashNode class as the basis for your linked
 * lists.
 * </p>
 *
 * @author Your Name
 */
public class HashMap<K,V> implements Map<K,V> {

    // Declare array(s) for storage here
    //
    // HINT: if you're using parallel arrays (one for keys and one for values),
    // declare them as type Object[].  You can cast the value back to the type
    // of K or V as necessary if you need to return them.
    //
    // If you don't do this, you'll have trouble creating tombstone values
    // because you won't know the type of the object to leave as a tombstone
    // at compile time.
    //
    // If you're using an array of HashNodes, you won't have this issue.
    // Simply declare your array as type HashNode[] (without the <K, V>)
    // and then cast it to HashNode<K, V>[] to work around the
    // generics.


    /**
     * <p>
     * Constructor: create a new empty map.
     * </p>
     *
     * <p>
     * <b>Precondition:</b> initial capacity must be a positive integer.
     * Load factor must be greater than 0.0 and less than or equal to 1.0.
     * </p>
     *
     * @param initialCapacity The initial capacity of the map
     * @param loadFactor The desired load factor of the map
     */
    public HashMap(int initialCapacity, double loadFactor) {
        if (initialCapacity < 1) {
            throw new IllegalArgumentException("Initial Capacity must be > 0");
        }
        if (loadFactor <= 0.0 || loadFactor > 1.0) {
            throw new IllegalArgumentException("0 < loadFactor <= 1.0");
        }

        // YOUR CODE HERE
    }


    /**
     * <p>
     * Constructor: create a new empty map with default capacity and
     * load factor.
     * </p>
     */
    public HashMap() {
        this(17, 0.75);
    }


    // HINT: You may want to make a method that takes a key as a parameter
    // and returns the location in the array for that key (based on the
    // hash value of the key and the length of the array).  You'll need
    // this operation for several of the other methods below.


    /**
     * @see Map#put(K, V) Map Interface Specification for put()
     */
    public V put(K key, V value) {
        return null;
    }


    /**
     * @see Map#get(K) Map Interface Specification for get()
     */
    public V get(K key) {
        return null;
    }


    /**
     * @see Map#remove(K) Map Interface Specification for remove()
     */
    public V remove(K key) {
        return null;
    }


    /**
     * @see Map#containsKey(K) Map Interface Specification for containsKey()
     */
    public boolean containsKey(K key) {
        return false;
    }


    /**
     * @see Map#size() Map Interface Specification for size()
     */
    public int size() {
        return -1;
    }


} // end of class HashMap
