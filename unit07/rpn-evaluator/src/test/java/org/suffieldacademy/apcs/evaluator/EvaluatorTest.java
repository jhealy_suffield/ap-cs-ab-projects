// $Id: EvaluatorTest.java 732 2020-01-30 14:34:50Z jhealy $
package org.suffieldacademy.apcs.evaluator;

import java.util.Scanner;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;


/**
 * <p>
 * <b>EvaluatorTest</b> checks the implementation of an RPN Evaluator
 * using some very basic tests.  You should add your own to make it better!
 * </p>
 *
 * @author Jason Healy
 * @author Your Name (if you add your own tests, put your name here)
 */
public class EvaluatorTest {

    // This is a helper method to run the actual test
    /**
     * Given an expected result value and an expression to test,
     * evaluate the expression and check its return value.
     *
     * @param expected The expected value after evaluating the expression
     * @param expression The expression to evaluate
     */
    public static void evaluate(double expected, String expression) {
        assertEquals("Evaluation of '"+expression+"' was incorrect;",
                     expected, Evaluator.evaluate(new Scanner(expression)),
                     0.0000001);
    }


    // If you want to define your own test, just copy one of these
    // (including the "@Test" line above), rename, and change the expression
    @Test
    public void alpha() {
        evaluate(4.5, "4 5 + 2 /");
    }

    @Test
    public void bravo() {
        evaluate(5, "3 5 - 7 +");
    }

    @Test
    public void charlie() {
        evaluate(6, "1 2 3 * + 1 3 + - 3 +");
    }

    @Test
    public void delta() {
        evaluate(-166, "12 14 + 13 / 12 14 * -");
    }


} // end of class EvaluatorTest
