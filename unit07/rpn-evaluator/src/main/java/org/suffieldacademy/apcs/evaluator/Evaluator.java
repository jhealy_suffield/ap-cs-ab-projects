package org.suffieldacademy.apcs.evaluator;


import java.util.Stack;
import java.util.EmptyStackException;

import java.util.Scanner;


/**
 * <p>
 * <b>Evaluator</b> takes a Scanner as its input and evaluates a mathematical
 * expression in postfix (aka "RPN") notation.  It returns the result of the
 * expression, or throws an exception if it cannot parse.
 * </p>
 *
 * @author Your Name
 */
public class Evaluator {


    /**
     * <p>
     * Evaluates a postfix expression, given as a string.  You should use
     * a Scanner to parse the string into numbers and operators; ignore
     * any unrecognized inputs.  You must support the basic four operators:
     * addition (+), subtraction (-), multiplication (*), and division (/).
     * </p>
     *
     * <p>
     * The Scanner should continue to parse until there are no more tokens
     * remaining (don't worry about lines or other boundaries; this is a
     * one-shot parse).
     * </p>
     *
     * <p>
     * If the expression cannot be parsed (the number of operators or
     * operands are not correct), you should throw an IllegalStateException
     * with an explanation of the error.  Otherwise, return the result of
     * the computation.
     * </p>
     *
     * @param exp The postfix expression to evaluate, wrapped in a Scanner
     *
     * @return double The result of evaluating the expression
     *
     * @throws IllegalStateException If the expression is invalid
     */
    public static double evaluate(Scanner exp) {
        // Hint: use java.util.Stack

        // Another hint: you can ask the Scanner if the next token is
        // going to be a double or not.  If it is, treat it as an operand.
        // If it isn't, treat it as an operator (and check to see if it's
        // + - * or /

        //throw new IllegalStateException("Method not written yet");
        return 0;
    }


} // end of class Evaluator
