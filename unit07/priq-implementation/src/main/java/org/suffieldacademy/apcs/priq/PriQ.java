package org.suffieldacademy.apcs.priq;


import java.util.ArrayList;


/**
 * <p>
 * <b>PriQ</b> implements the PriorityQueue interface using an
 * array- or ArrayList-backed data structure.  This class must implement a
 * min-priority PriorityQueue using a fixed-size array/list.
 * All add/remove operations must be logarithmic O(log n) time operations.
 * Because the queue has a fixed size, you should throw an exception when
 * trying to add an item to a full priq.
 * </p>
 *
 * @author Your Name
 *
 */
public class PriQ<E extends Comparable<? super E>> implements PriorityQueue<E> {

    // Your variables here.  You may either use a native array or
    // an ArrayList to hold your data.


    /**
     * Constructor: create an empty queue with the given capacity.
     */
    public PriQ(int capacity) {
        // Initialize your array or list (which you should declare above)

        // Arrays must be of type "Comparable" and then cast to type
        // E, like this:
        //someArray = (E[])(new Comparable[capacity]);

        // If you use an ArrayList, simply declare it to have the same
        // type as the PriQ:
        //someList = new ArrayList<E>(capacity);
    }


    /**
     * Constructor: create an empty queue with a default size.
     */
    public PriQ() {
        // you do not need to modify this constructor; only the non-default
        // one above

        // use the primary constructor and provide a default capacity
        this(100);
    }

    /**
     * Helper method.  This is NOT mandatory, but I suspect you'll find
     * it useful to write this method.  It should swap the elements at the
     * two given indices in the array(list).  Your implementation will vary
     * based on whether you're using an array or an ArrayList.
     *
     * @param j The index of the first element to swap
     * @param k The index of the second element to swap
     */
    private void swap(int j, int k) {
        // your code here
    }


    // FEEL FREE to borrow code from your HeapSort implementation.  You
    // won't need all of it (since you don't start with an unordered array),
    // but you will need to "re-heap" every time you add or remove an item...


    /**
     * postcondition: returns true if the number of elements is 0;
     *                otherwise, returns false
     */
    public boolean isEmpty() {
        return false; // REPLACE WITH YOUR CODE
    }


    /**
     * postcondition: x has been added to the priority queue;
     *                the number of elements in the priority
     *                queue is increased by 1.
     * exceptions: throws an unchecked exception if the
     *             priority queue is using an array to store
     *             its data and there is no room left in the array
     */
    public void add(E x) {
        // YOUR CODE HERE

        // hint: add the item to the end of your array, then
        // re-heap "up" (keep swapping with a parent as necessary)
        // until the array is a proper min-heap again.
    }


    /**
     * postcondition: The smallest item in the priority queue
     *                is removed and returned;
     *                the number of elements in the priority queue
     *                is decreased by 1.
     * exceptions: throws an unchecked exception if the
     *             priority queue is empty
    */
    public E removeMin() {
        return null; // REPLACE WITH YOUR CODE

        // hint: the item at position 0 is the smallest (root of the min-heap).
        // remove it and swap the last item in the array to the root,
        // then re-heap "down" (swap with the smaller of its children)
        // until the array is a proper min-heap again.
    }


    /**
     * postcondition: The smallest item in the priority queue
     *                is returned;
     *                the priority queue is unchanged
     * exceptions: throws an unchecked exception if the
     *             priority queue is empty
     */
    public E peekMin() {
        return null; // REPLACE WITH YOUR CODE
    }


} // end of class PriQ
