package org.suffieldacademy.apcs.priq;


/**
 * <p>
 * <b>PriorityQueue</b> interface from the AP subset.  Based on a simplified
 * version of java.util.PriorityQueue.
 * </p>
 *
 * Last Modified $Date: 2020-01-30 09:34:50 -0500 (Thu, 30 Jan 2020) $ by $Author: jhealy $
 *
 */
public interface PriorityQueue<E> {

    /**
     * postcondition: returns true if the number of elements is 0;
     *                otherwise, returns false
     */
    public boolean isEmpty();

    /**
     * postcondition: x has been added to the priority queue;
     *                the number of elements in the priority
     *                queue is increased by 1.
     */
    public void add(E x);

    /**
     * postcondition: The smallest item in the priority queue
     *                is removed and returned;
     *                the number of elements in the priority queue
     *                is decreased by 1.
     * exceptions: throws an unchecked exception if the
     *             priority queue is empty
    */
    public E removeMin();

    /**
     * postcondition: The smallest item in the priority queue
     *                is returned;
     *                the priority queue is unchanged
     * exceptions: throws an unchecked exception if the
     *             priority queue is empty
     */
    public E peekMin();

} // end of interface PriorityQueue
