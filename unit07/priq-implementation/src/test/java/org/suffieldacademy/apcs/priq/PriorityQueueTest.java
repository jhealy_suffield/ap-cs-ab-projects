// $Id: PriorityQueueTest.java 732 2020-01-30 14:34:50Z jhealy $
package org.suffieldacademy.apcs.priq;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;


/**
 * <p>
 * <b>PriorityQueueTest</b> checks the implementation of PriQ to ensure
 * that it performs basic operations correctly.
 * </p>
 *
 * @author Jason Healy
 */
public class PriorityQueueTest {

    /** PriorityQueue interface variable */
    protected PriorityQueue<String> pq = null;

    /** Subclass should use this method to initialize the variable above. */
    @Before
    public void beforeEachTest() {
        pq = new PriQ<String>();
    }

    /** Set the variable back to null after each test. */
    @After
    public void afterEachTest() {
        pq = null;
    }

    @Test(expected=RuntimeException.class)
    public void removeEmpty() {
        pq.removeMin();
    }


    @Test(expected=RuntimeException.class)
    public void peekEmpty() {
        pq.peekMin();
    }


    @Test
    public void isEmpty() {
        assertTrue("Empty priq should be empty", pq.isEmpty());
        pq.add("A");
        assertFalse("Priq after add should not be empty", pq.isEmpty());
    }


    @Test
    public void loneAddRemove() {

        String a = "A";
        pq.add(a);
        assertEquals("Lone item should be returned by peekMin()",
                     a, pq.peekMin());
        assertEquals("Lone item should be returned by removeMin()",
                     a, pq.removeMin());
    }


    @Test
    public void addRemoveEmpty() {
        pq.add("A");
        pq.removeMin();
        assertTrue("Priq should be empty after add/removeMin combo", pq.isEmpty());
    }


    @Test
    public void reverseTest() {
        // add items in reverse order to ensure that priq is sorting
        // them before returning them

        int start = 10;

        try {
            for (int i = 9; i >= 0; i--) {
                pq.add(""+i);
                start = i;
            }
        }
        catch (IllegalStateException ise) {
            // priq must be full, so that's as far as we can go
        }

        for (int i = start; i < 10; i++) {
            assertEquals("removeMin() item not correct",
                         ""+i, pq.removeMin());
        }
    }


    @Test
    public void randomStressTest() {
        final int maxRuns = 10000;

        java.util.PriorityQueue<String> j =
            new java.util.PriorityQueue<String>(100);

        java.util.Random r = new java.util.Random();

        int run = 0;
        while (run < maxRuns || j.size() > 0) {
            if (run < maxRuns && r.nextBoolean()) { // add
                try {
                    String random = ""+r.nextInt(99999);
                    //System.out.println("nq " +  random);
                    pq.add(random);
                    j.offer(random);
                }
                catch (IllegalStateException ise) {
                    // priq must be full, so just carry on
                }
            }
            else { // remove
                try {
                    String sq = pq.removeMin();
                    String sj = j.poll();
                    //System.out.println("dq " + sj);

                    assertEquals("removeMin() item not correct",
                     sj, sq);
                }
                catch (RuntimeException re) {
                    // priq must be empty, make sure java priq is too
                    if (j.size() > 0) {
                        fail("Could not remove from a priq, got a " + re
                             + ".  However, the priq shouldn't be empty so I wasn't expecting an exception.");
                    }
                }
            }
            run++;
        }
    }


} // end of class PriorityQueueTest
