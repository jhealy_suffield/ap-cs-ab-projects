package org.suffieldacademy.apcs.linkedlist;

/**
 * <p>
 * <b>LinkedList</b> implements the List interface.  You may use any type of
 * linked structure (single, double, circular, etc), so long as you correctly
 * implement the methods of the List interface.
 * </p>
 *
 * <p>
 * Note the private inner <b>Node</b> class used to represent the nodes
 * of the list.  You may need to make changes to it if you structure the
 * list differently than the default (doubly-linked).
 * </p>
 *
 * @author Your Name
 *
 */
public class LinkedList<E> implements List<E> {

    /** Head of the linked list */
    private Node<E> head;


    /**
     * <p>
     * Constructor.  Creates a new empty list.
     * </p>
     */
    public LinkedList() {
    }


    /**
     * <p>
     * Returns the size (number of elements) in the list.  This should be
     * an O(1) operation.
     * </p>
     *
     * @return int The number of items in the list
     */
    public int size() {
        return Integer.MIN_VALUE;
    }


    /**
     * <p>
     * Adds the given object to the end (tail) of the list, and increases
     * the size of the list by 1.  This should be an O(1) operation.
     * </p>
     *
     * @param o The object to add to the list
     *
     * @return boolean True, if the item was added to the list (should always be true)
     */
    public boolean add(E o) {
        return false;
    }


    /**
     * <p>
     * Adds the given object to the given position of the list.  If there
     * is already an item at this position, it and any subsequent nodes
     * are moved to a higher position and this new node is inserted before
     * them.  This should be an O(n) operation.
     * </p>
     *
     * <p>
     * If the position to insert is beyond the end of the list, or if it is
     * less than zero, an IllegalArgumentException should be thrown.
     * </p>
     *
     * @param o The object to add to the list
     * @param i The position in the list to add to
     *
     * @return boolean True, if the item was added to the list (should always be true)
     */
    public boolean add(E o, int i) {
        return false;
    }


    /**
     * <p>
     * Removes all instances of the given item from the list.  If the item
     * does not exist in the list, no changes to the list are made.
     * This should be an O(n) operation.  Use .equals() for the equality
     * check, not ==.
     * </p>
     *
     * @param o The object to remove from the list
     */
    public void removeAll(E o) {
    }


    /**
     * <p>
     * Removes the item at the given position of the list.  If the position
     * given is beyond the end of the list, or less than zero, an
     * IllegalArgumentException should be thrown.  This should be an
     * O(n) operation.
     * </p>
     *
     * @param i The position of the item to remove
     */
    public void remove(int i) {
    }


    /**
     * <p>
     * Returns the item at the given position of the list, but does not
     * modify the list in any way.  If the position given is beyond the
     * end of the list, or less than zero, an IllegalArgumentException
     * should be thrown.  This should be an O(n) operation.
     * </p>
     *
     * @param i The index of the item to retrieve
     *
     * @return E The element at the given position
     */
    public E get(int i) {
        return null;
    }


    /**
     * <p>
     * Replaces the item at the given position in the list with the new
     * given item.  The method returns the previous value that was
     * associated with the node.  The length and ordering of the list
     * remains unchanged.  If the position given is beyond the end of
     * the list, or less than zero, an IllegalArgumentException should
     * be thrown.  This should be an O(n) operation.
     * </p>
     *
     * @param o The new value to set
     * @param i The position of the item to modify
     *
     * @return E The old value that was replaced.
     */
    public E set(E o, int i) {
        return null;
    }


    /**
     * <p>
     * Returns an array representation of the list.  The array should be
     * sized to exactly fit all the items.  If the list is empty, a zero-
     * length array should be returned.
     * </p>
     *
     * @return Object[] An array representation of the list
     */
    public Object[] toArray() {
        return null;
    }

    /**
     *
     * <p>
     * <code>Node</code> is a private inner class used by the linked
     * list to store its data and the list structure.  Because it is
     * declared inside the LinkedList class, you can directly access
     * the private instance variables of the class.
     * </p>
     *
     * <p>
     * The default implementation represents a doubly-linked node.
     * If you wish to use a different type of node, make any necessary
     * changes to the instance variables and constructor below.
     * </p>
     *
     * @author Jason Healy
     *
     * @param <E> The type of data held by the node; must match the
     * enclosing list
     */
    private class Node<E> {

        private E data;
        private Node<E> prev;
        private Node<E> next;

        /**
         * <p>
         * Creates a new list node holding the given data, and referring to
         * the given previous and next nodes in the list.
         * </p>
         *
         * @param data The object to store in this node
         * @param prev The previous node in the list
         * @param next The next node in the list
         */
        public Node(E data, Node<E> prev, Node<E> next) {
            this.data = data;
            this.prev = prev;
            this.next = next;
        }

        // No methods are necessary; you may access the private fields
        // directly by using "node.data" syntax.

    } // end of class Node

} // end of class LinkedList
