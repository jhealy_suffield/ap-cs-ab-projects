package org.suffieldacademy.apcs.linkedlist;

/**
 * <p>
 * <b>List</b> specifies the methods required of your list class.  You
 * do not need to (and should not) edit this file.
 * </p>
 *
 * @author Jason Healy
 * @version $Revision: 724 $
 *
 * Last Modified $Date: 2018-01-28 15:01:20 -0500 (Sun, 28 Jan 2018) $ by $Author: jhealy $
 *
 */
public interface List<E> {


    /**
     * <p>
     * Returns the size (number of elements) in the list.  This should be
     * an O(1) operation.
     * </p>
     *
     * @return int The number of items in the list
     */
    public int size();


    /**
     * <p>
     * Adds the given object to the end (tail) of the list, and increases
     * the size of the list by 1.  This should be an O(1) operation.
     * </p>
     *
     * @param o The object to add to the list
     *
     * @return boolean True, if the item was added to the list
     */
    public boolean add(E o);


    /**
     * <p>
     * Adds the given object to the given position of the list.  If there
     * is already an item at this position, it and any subsequent nodes
     * are moved to a higher position and this new node is inserted before
     * them.  This should be an O(n) operation.
     * </p>
     *
     * <p>
     * If the position to insert is larger than the size of the list, or if
     * it is less than zero, an IllegalArgumentException should be thrown.
     * Note that inserting at position == size is valid.
     * </p>
     *
     * @param o The object to add to the list
     * @param i The position in the list to add to
     *
     * @return boolean True, if the item was added to the list
     */
    public boolean add(E o, int i);


    /**
     * <p>
     * Removes all instances of the given item from the list.  If the item
     * does not exist in the list, no changes to the list are made.
     * This should be an O(n) operation.  Use .equals() for the equality
     * check, not ==.
     * </p>
     *
     * @param o The object to remove from the list
     */
    public void removeAll(E o);


    /**
     * <p>
     * Removes the item at the given position of the list.  If the position
     * given is beyond the end of the list, or less than zero, an
     * IllegalArgumentException should be thrown.  This should be an
     * O(n) operation.
     * </p>
     *
     * @param i The position of the item to remove
     */
    public void remove(int i);


    /**
     * <p>
     * Returns the item at the given position of the list, but does not
     * modify the list in any way.  If the position given is beyond the
     * end of the list, or less than zero, an IllegalArgumentException
     * should be thrown.  This should be an O(n) operation.
     * </p>
     *
     * @param i The index of the item to retrieve
     *
     * @return E The element at the given position
     */
    public E get(int i);


    /**
     * <p>
     * Replaces the item at the given position in the list with the new
     * given item.  The method returns the previous value that was
     * associated with the node.  The length and ordering of the list
     * remains unchanged.  If the position given is beyond the end of
     * the list, or less than zero, an IllegalArgumentException should
     * be thrown.  This should be an O(n) operation.
     * </p>
     *
     * @param o The new value to set
     * @param i The position of the item to modify
     *
     * @return E The old value that was replaced.
     */
    public E set(E o, int i);


    /**
     * <p>
     * Returns an array representation of the list.  The array should be
     * sized to exactly fit all the items.  If the list is empty, a zero-
     * length array should be returned.
     * </p>
     *
     * @return Object[] An array representation of the list
     */
    public Object[] toArray();


} // end of interface List
