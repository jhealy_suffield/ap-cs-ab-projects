// $Id: ListTest.java 724 2018-01-28 20:01:20Z jhealy $
package org.suffieldacademy.apcs.linkedlist;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * <p>
 * <b>ListTest</b> checks the implementation of List to ensure
 * that it performs all operations correctly.
 * </p>
 *
 * @author Jason Healy
 */
public class ListTest {

    @Test
    public void testEmpty() {

        List<String> l = new LinkedList<String>();

        assertEquals("A new list should have a size of zero",
                     0, l.size());

        try {
            l.get(0);
            fail("Empty list should not allow get(0)");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("get(0) on empty list threw unexpected exception: " + e);
        }

        try {
            l.get(-1);
            fail("List should not allow get(-1)");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("get(-1) threw unexpected exception: " + e);
        }

        try {
            l.get(l.size() + 1);
            fail("List should not allow get(list.size()+1)");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("get(list.size()+1) threw unexpected exception: " + e);
        }

    }


    @Test
    public void testOne() {

        List<String> l = new LinkedList<String>();
        String zero = "Zero";

        // add
        try {
            assertTrue("Successfully adding an element should return true",
                    l.add(zero));
        }
        catch (Exception e) {
            fail("add() threw unexpected exception: " + e);
        }

        assertEquals("List after adding single element has wrong size",
                     1, l.size());

        String zeroGet = null;

        // get added
        try {
            zeroGet = l.get(0);
        }
        catch (Exception e) {
            fail("get(0) threw unexpected exception: " + e);
        }

        assertSame("List after adding single element did not return added element in get()",
                     zero, zeroGet);


        // set
        zero = "ZeroNew";

        try {
            l.set(zero, 0);
        }
        catch (Exception e) {
            fail("set(0) threw unexpected exception: " + e);
        }

        assertEquals("List after modifying single element has wrong size",
                     1, l.size());

        zeroGet = null;

        // get set
        try {
            zeroGet = l.get(0);
        }
        catch (Exception e) {
            fail("get(0) threw unexpected exception: " + e);
        }

        assertSame("List after modifying single element did not return added element in get()",
                     zero, zeroGet);


        // remove
        try {
            l.remove(0);
        }
        catch (Exception e) {
            fail("remove(0) threw unexpected exception: " + e);
        }

        assertEquals("List after removing single element has wrong size",
                     0, l.size());

        zeroGet = null;

        // get remove
        try {
            zeroGet = l.get(0);
            fail("get(0) after removing only element should throw exception");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("get(0) on removed list threw unexpected exception: " + e);
        }

    }

    public static String arrToStr(Object[] a) {
        StringBuilder sb = new StringBuilder(a.length * 4);

        if (a.length > 0) {
            sb.append(a[0]);
        }

        for (int i = 1; i < a.length; i++) {
            sb.append(", ");
            sb.append(a[i]);
        }

        return sb.toString();
    }


    @Test
    public void testArray() {
        List<String> l = new LinkedList<String>();
        java.util.LinkedList<String> j = new java.util.LinkedList<String>();

        for (int i = 0; i < 10; i++) {
            l.add(""+i);
            j.add(""+i);
        }

        Object[] la = l.toArray();
        Object[] ja = j.toArray();

        assertNotNull("toArray() should not return null", la);

        assertEquals("List -> array does not have correct size",
                     ja.length, la.length);

        assertEquals("List -> array does not contain correct elements",
                     arrToStr(ja), arrToStr(la));
    }


    @Test
    public void testManyAppend() {
        List<String> l = new LinkedList<String>();
        java.util.LinkedList<String> j = new java.util.LinkedList<String>();

        for (int i = 0; i < 10; i++) {
            l.add(""+i);
            j.add(""+i);

            assertEquals("Just-added element " + i + " not returned by get("+i+")",
                         j.get(i), l.get(i));
        }

        assertEquals("List after 10 add-to-end does not have correct size",
                     j.size(), l.size());

        assertEquals("List after 10 add-to-end does not contain correct elements",
                     arrToStr(j.toArray()), arrToStr(l.toArray()));
    }


    @Test
    public void testManyInsert() {
        List<String> l = new LinkedList<String>();
        java.util.LinkedList<String> j = new java.util.LinkedList<String>();

        l.add(""+10);
        j.add(""+10);

        for (int i = 0; i < 10; i++) {
            l.add(""+i, i);
            j.add(i, ""+i);

            assertEquals("Just-added element " + i + "th element " + i + " not returned by get("+i+")",
                         j.get(i), l.get(i));
        }

        assertEquals("List after ordered inserts does not have correct size",
                     j.size(), l.size());

        assertEquals("List after ordered inserts does not contain correct elements",
                     arrToStr(j.toArray()), arrToStr(l.toArray()));

    }


    @Test
    public void testManySet() {
        List<String> l = new LinkedList<String>();
        java.util.LinkedList<String> j = new java.util.LinkedList<String>();

        for (int i = 0; i > -10; i--) {
            l.add(""+i);
            j.add(""+i);
        }

        for (int i = 0; i < 10; i++) {
            l.set(""+i, i);
            j.set(i, ""+i);

            assertEquals("Just-set " + i + "th element " + i + " not returned by get("+i+")",
                         j.get(i), l.get(i));
        }

        assertEquals("List after set does not have correct size",
                     j.size(), l.size());

        assertEquals("List after set does not contain correct elements",
                     arrToStr(j.toArray()), arrToStr(l.toArray()));

    }


    @Test
    public void testOrderedRemove() {
        List<String> l = new LinkedList<String>();
        java.util.LinkedList<String> j = new java.util.LinkedList<String>();

        for (int i = 0; i < 10; i++) {
            l.add(""+i);
            j.add(""+i);
        }

        for (int i = 0; i < 5; i++) {
            l.remove(0);
            j.remove(0);
        }

        assertEquals("List after removing half does not have correct size",
                     j.size(), l.size());

        assertEquals("List after removing half does not contain correct elements",
                     arrToStr(j.toArray()), arrToStr(l.toArray()));

        for (int i = 5; i < 10; i++) {
            l.remove(0);
            j.remove(0);
        }

        assertEquals("List after removing all does not have correct size",
                     j.size(), l.size());

        assertEquals("List after removing all does not contain correct elements",
                     arrToStr(j.toArray()), arrToStr(l.toArray()));

    }


    @Test
    public void testSkipRemove() {
        List<String> l = new LinkedList<String>();
        java.util.LinkedList<String> j = new java.util.LinkedList<String>();

        for (int i = 0; i < 10; i++) {
            l.add(""+i);
            j.add(""+i);
        }

        for (int i = 9; i >= 0; i-=2) {
            l.remove(i);
            j.remove(i);
        }

        assertEquals("List after removing every other does not have correct size",
                     j.size(), l.size());

        assertEquals("List after removing every other does not contain correct elements",
                     arrToStr(j.toArray()), arrToStr(l.toArray()));

    }


    @Test
    public void testRemoveAll() {
        List<String> l = new LinkedList<String>();
        java.util.LinkedList<String> j = new java.util.LinkedList<String>();

        String bogus = "-1";

        for (int i = 0; i < 20; i++) {
            if (i % 2 == 0) {
                l.add(bogus);
            }
            else {
                l.add(""+i);
                j.add(""+i);
            }
        }

        l.removeAll(bogus);

        assertEquals("List after removing all " + bogus + " does not have correct size",
                     j.size(), l.size());

        assertEquals("List after removing all " + bogus + " does not contain correct elements",
                     arrToStr(j.toArray()), arrToStr(l.toArray()));

    }


    @Test
    public void testBoundsAddLower() {
        List<String> l = new LinkedList<String>();

        l.add("A");
        l.add("B");
        l.add("C");

        try {
            l.add("X", -1); // out of bounds
            fail("List should not allow add(o, -1)");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("add(o, -1) threw unexpected exception: " + e);
        }

    }


    @Test
    public void testBoundsAddUpper() {
        List<String> l = new LinkedList<String>();

        l.add("A");
        l.add("B");
        l.add("C");

        try {
            l.add("X", l.size()+1); // out of bounds
            fail("List should not allow add(o, list.size()+1); you can add to the next empty position, but not beyond");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("add(o, list.size()+1) threw unexpected exception: " + e);
        }
    }


    @Test
    public void testBoundsGetLower() {
        List<String> l = new LinkedList<String>();

        l.add("A");
        l.add("B");
        l.add("C");

        try {
            l.get(-1); // out of bounds
            fail("List should not allow get(-1)");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("get(-1) threw unexpected exception: " + e);
        }
    }


    @Test
    public void testBoundsGetUpper() {
        List<String> l = new LinkedList<String>();

        l.add("A");
        l.add("B");
        l.add("C");

        try {
            l.get(l.size()); // out of bounds
            fail("List should not allow get(list.size()); a zero-based index means that size()-1 is the last index");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("get(list.size()) threw unexpected exception: " + e);
        }
    }


    @Test
    public void testBoundsSetLower() {
        List<String> l = new LinkedList<String>();

        l.add("A");
        l.add("B");
        l.add("C");

        try {
            l.set("X", -1); // out of bounds
            fail("List should not allow set(-1)");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("set(-1) threw unexpected exception: " + e);
        }
    }


    @Test
    public void testBoundsSetUpper() {
        List<String> l = new LinkedList<String>();

        l.add("A");
        l.add("B");
        l.add("C");

        try {
            l.set("X", l.size()); // out of bounds
            fail("List should not allow set(list.size()); a zero-based index means that size()-1 is the last index");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("set(list.size()) threw unexpected exception: " + e);
        }
    }


    @Test
    public void testBoundsRemoveLower() {
        List<String> l = new LinkedList<String>();

        l.add("A");
        l.add("B");
        l.add("C");

        try {
            l.remove(-1); // out of bounds
            fail("List should not allow remove(-1)");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("remove(-1) threw unexpected exception: " + e);
        }
    }


    @Test
    public void testBoundsRemoveUpper() {
        List<String> l = new LinkedList<String>();

        l.add("A");
        l.add("B");
        l.add("C");

        try {
            l.remove(l.size()); // out of bounds
            fail("List should not allow remove(list.size()); a zero-based index means that size()-1 is the last index");
        }
        catch (IllegalArgumentException iae) {}
        catch (Exception e) {
            fail("remove(list.size()) threw unexpected exception: " + e);
        }
    }


    @Test
    public void testRandomStressTest() {
        List<String> l = new LinkedList<String>();
        java.util.LinkedList<String> j = new java.util.LinkedList<String>();

        for (int i = 0; i < 10; i++) {
            l.add(""+i);
            j.add(""+i);
        }

        assertEquals("List size incorrect after adding test elements",
                10, l.size());

        java.util.ArrayList<String> vals = new java.util.ArrayList<String>();

        java.util.Random r = new java.util.Random();

        for (int i = 0; i < 100; i++) {
            int pos1 = r.nextInt(l.size());
            int pos2 = r.nextInt(l.size());
            int ran1 = r.nextInt(100);
            String val1 = ""+pos1;
            String val2 = ""+ran1;

            vals.add(val1);
            vals.add(val2);

            l.add(val1, pos1);
            j.add(pos1, val1);

            assertEquals("List after random pos add does not have correct size",
                         j.size(), l.size());

            assertEquals("List after random pos add does not contain correct elements",
                         arrToStr(j.toArray()), arrToStr(l.toArray()));

            l.add(val2, pos2);
            j.add(pos2, val2);

            assertEquals("List after random val add does not have correct size",
                         j.size(), l.size());

            assertEquals("List after random val add does not contain correct elements",
                         arrToStr(j.toArray()), arrToStr(l.toArray()));

            int rem1 = r.nextInt(l.size());
            l.remove(rem1);
            j.remove(rem1);

            assertEquals("List after random pos remove does not have correct size",
                         j.size(), l.size());

            assertEquals("List after random pos remove does not contain correct elements",
                         arrToStr(j.toArray()), arrToStr(l.toArray()));

            int rem2 = r.nextInt(vals.size());
            l.removeAll(vals.get(rem2));
            while (j.remove(vals.get(rem2)));
            vals.remove(rem2);

            assertEquals("List after random val removeAll does not have correct size",
                         j.size(), l.size());

            assertEquals("List after random val removeAll does not contain correct elements",
                         arrToStr(j.toArray()), arrToStr(l.toArray()));

        }
    }

    // the overloaded helper fail(msg, throwable) exists in JUnit 5,
    // but we're only on 4, so faking it here.  Remove when you upgrade.
    public static void failThrowable(String msg, Throwable t) {
        String trace = "\n" + t + "; stack trace:";
        for (StackTraceElement ste : t.getStackTrace()) {
            trace += "\n" + ste;
        }

        fail(msg+trace);
    }

    @Test
    public void testAddPositionalTail() {
        List<String> l = new LinkedList<String>();
        l.add("Zero");
        l.add("One");

        try {
            l.add("Added at pos=2 when size=2", 2);
        }
        catch (Exception e) {
            failThrowable("Trying to perform a positional add where position==size but got an exception.  Are you incorrectly rejecting this?", e);
        }

        l.add("Added at tail when size=3");

        assertEquals("List after adding four elements has wrong size",
                     4, l.size());

        assertEquals("List after a positional add(size) did not return correct value; are you sure your tail reference is correct after a positional add?",
                   "Added at pos=2 when size=2", l.get(2));

        assertEquals("List after a positional add(size) and tail add did not return correct value; are you sure your tail reference is correct after a positional add?",
                   "Added at tail when size=3", l.get(3));


    }

    @Test
    public void testRemovePositionalTail() {
        List<String> l = new LinkedList<String>();
        l.add("Zero");
        l.add("One");
        l.add("Two before remove");

        try {
            l.remove(2);
        }
        catch (Exception e) {
            failThrowable("Trying to remove the last (tail) node in the list threw unexpected exception", e);
        }

        l.add("Two after remove");

        assertEquals("List after adding four and removing one elements has wrong size",
                     3, l.size());

        String got = null;

        try {
            got = l.get(2);
        }
        catch (Exception e) {
            failThrowable("I removed the tail element and added a new element to the tail, but when I tried to fetch the new value I got an unexpected exception.  The list structure may be incorrect after removing the tail node; are you sure you're updating tail correctly when this happens?", e);
        }

        assertEquals("I removed the tail element and added a new element to the tail, and did not get back the correct value for the new tail; are you sure your tail reference is correct if the tail node is deleted?",
                   "Two after remove", got);

    }

    @Test
    public void testRemoveAllTail() {
        // exactly the same setup as positional remove above

        List<String> l = new LinkedList<String>();
        l.add("Zero");
        l.add("One");
        l.add("Two before remove");

        try {
            l.removeAll("Two before remove");
        }
        catch (Exception e) {
            failThrowable("Trying to removeAll when the last (tail) node matches threw unexpected exception", e);
        }

        l.add("Two after remove");

        assertEquals("List after adding four and removing one elements has wrong size",
                     3, l.size());

        String got = null;

        try {
            got = l.get(2);
        }
        catch (Exception e) {
            failThrowable("I removed the tail element and added a new element to the tail, but when I tried to fetch the new value I got an unexpected exception.  The list structure may be incorrect after removing the tail node; are you sure you're updating tail correctly when this happens?", e);
        }

        assertEquals("I removed the tail element and added a new element to the tail, and did not get back the correct value for the new tail; are you sure your tail reference is correct if the tail node is deleted?",
                   "Two after remove", got);

    }



} // end of class ListTest
