import java.util.Scanner;
import java.util.NoSuchElementException;


/**
 *
 * <p>
 * <b>ScannerCalc</b> is a simple program that reads in tokens using the
 * Scanner class, and attempts to parse them into an arithmetic expression.
 * It then solves the expression and prints the answer out for the user.
 * </p>
 *
 * <p>
 * I have included a sample solution for this lab.  To run it, open a
 * terminal window in this folder and type:
 *
 *  java -jar ScannerCalc_solution.jar
 *
 * </p>
 *
 * <p>
 * Your solution should mimic mine, including input validation (it shouldn't
 * throw any exceptions), and supported number of operands (you only need
 * to accept 2 operands and 1 operator per line).
 * </p>
 *
 * @author Your Name Here
 */
public class ScannerCalc {


    /**
     * Standard method to run when invoked by Java VM.
     */
    public static void main(String[] args) {

        // put your name in the comments above!!

        System.out.println("Nothing to do!");

        // you'll want to erase the body of this method and replace it
        // with functioning code that operates like the solution.

        // You may find the following helpful to look up what the Scanner
        // and Formatter classes do:

        // http://download.oracle.com/javase/tutorial/essential/io/scanning.html
        // http://download.oracle.com/javase/tutorial/essential/io/formatting.html

        // http://leepoint.net/notes-java/summaries/summary-scanner.html
        // http://www.java2s.com/Tutorial/Java/0120__Development/UsingJavasprintfMethod.htm

        // http://download.oracle.com/javase/7/docs/api/java/util/Scanner.html
        // http://download.oracle.com/javase/7/docs/api/java/util/Formatter.html

        // Don't forget to check for malformed input (e.g., strings when
        // you should get a number), and also be sure to catch any
        // NoSuchElementExceptions that could happen at any time (if the
        // user hits control-D).

    }
}
