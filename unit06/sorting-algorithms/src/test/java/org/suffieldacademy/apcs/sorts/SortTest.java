package org.suffieldacademy.apcs.sorts;

import static org.junit.Assert.*;
import org.junit.Test;


/**
 * <p>
 * <code>SortTester</code> provides methods to verify that a given sorting
 * algorithm functions correctly.  I've included a basic test, but you should
 * write your own tests to verify any other special cases.
 * </p>
 *
 * @author Your Name
 *
 */
public class SortTest {

    /** Define the implementing class that will perform the sorting */
    protected static Sorter sorter = new BubbleSort();


    /**
     * <p>
     * Tests the given array to ensure that it is sorted in ascending order
     * per the natural ordering of its elements (using each element's
     * compareTo() method).  If it is not, the method fails via a JUnit
     * assertion.
     * </p>
     *
     * <p>
     * This method should be used inside of a test fixture to check if an
     * array is properly sorted.  If this method does <b>not</b> fail, then
     * the test may assume the array is properly sorted.
     * </p>
     *
     * <p>
     * <b>Precondition: a contains only comparable elements</b>
     * </p>
     *
     * <p>
     * <b>Postcondition: the method completes successfully (without throwing
     * any errors) iff all elements in the array are sorted in ascending order.</b>
     * </p>
     *
     * @param <T> Any type that implements the {@link Comparable Comparable} interface
     * @param a The array of elements to test
     * @param min The minimum value to verify
     * @param max The first value NOT to verify (that is, verify only to max-1)
     */
    public static <T extends Comparable<? super T>> void verifyOrder(T[] a,
                                                                     int min,
                                                                     int max) {
        for (int i = min+1; i < max; i++) {
            if (a[i].compareTo(a[i-1]) < 0) {
                System.out.println("Sort failed; see failure message for array dump");
                //sorter.dump(a, min, max);
                fail("Array not sorted; items at positions ["
                    + (i-1) + "] and [" + i
                    + "] are not in relative order\n"
                    + sorter.stringify(a, min, max)
                    + "\n\n");
            }
        }
    }


    /** Convenience method to verify an entire array */
    public static <T extends Comparable<? super T>> void verifyOrder(T[] a) {
        verifyOrder(a, 0, a.length);
    }


    /**
     * <p>
     * Constructs a basic String array, sorts it, then confirms the array
     * is sorted correctly.
     * </p>
     *
     * <p>
     * You can make copies of this method (change the name of the copy, though)
     * and initialize different arrays to test the functionality of your sort).
     * Be sure to include the "@Test" annotation on the line above the method
     * declaration and JUnit will automatically find and run it.
     * </p>
     */
    @Test
    public void testSortStringArray() {
        // We use Strings here to make it easier to tell the values
        // from their indices
        String[] a = { "J", "A", "I", "B", "H", "C", "G", "D", "F", "E" };

        // Have our test sorter sort it
        sorter.sort(a);

        // Fail if it isn't in order
        verifyOrder(a);
    }


    /**
     * <p>
     * Sorts a range of values, but not the entire array.  Makes sure that
     * the sort does not modify values outside the requested range, and
     * also that it sorts correctly.
     * </p>
     */
    @Test
    public void testSortPartialArray() {
        // create an array of values (note the use of capital-I Integer
        // instead of int, as the values must be Comparable)
        Integer[] a = new Integer[30];
        int min = a.length / 3;
        int max = min * 2;

        // Put a bunch of canary values outside the desired sorting range
        // that shouldn't be touched
        for (int i = 0; i < min; i++) {
            a[i] = Integer.MAX_VALUE;
        }
        for (int i = max; i < a.length; i++) {
            a[i] = Integer.MIN_VALUE;
        }

        // fill the middle of the array with actual values
        for (int i = min; i < max; i++) {
            a[i] = max-i;
        }

        // Have our test sorter sort it
        sorter.sort(a, min, max);

        // Fail if the range isn't in order
        verifyOrder(a, min, max);

        // Also make sure the sort didn't touch values outside the range
        for (int i = 0; i < min; i++) {
            assertTrue("Value at a[" + i + "] was changed, but the sort range was " + min + " (inclusive) - " + max + " (exclusive).\nMake sure you aren't accessing values with index less than 'min' or greater than/equal to 'max'.",
                         Integer.MAX_VALUE == a[i]);
        }
        for (int i = max; i < a.length; i++) {
            assertTrue(
                    "Value at a[" + i + "] was changed, but the sort range was " + min + " (inclusive) - " + max
                            + " (exclusive).\nMake sure you aren't accessing values with index less than 'min' or greater than/equal to 'max'.",
                    Integer.MIN_VALUE == a[i]);
        }
        for (int i = min; i < max; i++) {
            assertTrue(
                    "Value at a[" + i + "] has a value that did not exist in the sort range " + min + " (inclusive) - " + max
                            + " (exclusive).\nMake sure you aren't accessing values with index less than 'min' or greater than/equal to 'max'.",
                    Integer.MIN_VALUE != a[i] && Integer.MAX_VALUE != a[i]);
        }
    }


    /**
     * <p>
     * Test an array that is in reverse order.
     * </p>
     */
    @Test
    public void testReverseArray() {
        // We use Strings here to make it easier to tell the values
        // from their indices
        String[] a = { "J", "I", "H", "G", "F", "E", "D", "C", "B", "A" };

        // Have our test sorter sort it
        sorter.sort(a);

        // Fail if it isn't in order
        verifyOrder(a);
    }


    /**
     * <p>
     * Test an array that is already in order.
     * </p>
     */
    @Test
    public void testSortedArray() {
        // We use Strings here to make it easier to tell the values
        // from their indices
        String[] a = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };

        // Have our test sorter sort it
        sorter.sort(a);

        // Fail if it isn't in order
        verifyOrder(a);
    }


    /**
     * <p>
     * Test an array with duplicates.
     * </p>
     */
    @Test
    public void testDuplicatesArray() {
        // We use Strings here to make it easier to tell the values
        // from their indices
        String[] a = { "A", "B", "C", "D", "E", "F", "A", "E", "C" };

        // Have our test sorter sort it
        sorter.sort(a);

        // Fail if it isn't in order
        verifyOrder(a);
    }


    /**
     * <p>
     * Test an array that only has one element.
     * </p>
     */
    @Test
    public void testSmallArray() {
        // We use Strings here to make it easier to tell the values
        // from their indices
        String[] a = { "X" };

        // Have our test sorter sort it
        sorter.sort(a);

        // Fail if it isn't in order
        verifyOrder(a);
    }


    /**
     * <p>
     * Test an array that is empty/
     * </p>
     */
    @Test
    public void testEmptyArray() {
        // We use Strings here to make it easier to tell the values
        // from their indices
        String[] a = new String[0];

        // Have our test sorter sort it
        sorter.sort(a);

        // Fail if it isn't in order
        verifyOrder(a);
    }


    /**
     * <p>
     * Test a large array with randomly-generated contents
     * </p>
     */
    @Test
    public void testLargeRandomArray() {
        String[] a = new String[1000];

        // Fill each space in the array with a random string of
        // capital letters (26 possibilities per char)
        for (int i = 0; i < a.length; i += 1) {
            byte[] chr = new byte[3];
            for (int c = 0; c < chr.length; c += 1) {
                // In ASCII, A is 65, so skew a random number distribution
                // by 65 to get a range of 65-90 (A-Z)
                chr[c] = (byte)((Math.random() * 26) + 65);
            }
            a[i] = new String(chr);
        }

        // Have our test sorter sort it
        sorter.sort(a);

        // Fail if it isn't in order
        verifyOrder(a);
    }


    // Your code here.  Feel free to create additional tests using
    // the ones above as a template.

}
