package org.suffieldacademy.apcs.sorts;


/**
 * <p>
 * <code>Sorter</code> defines a sort method, allowing implementing classes to
 * advertise the ability to sort values.  It also includes several default
 * implementation methods that are helpful when sorting (you do not need to
 * edit or implement these methods yourself unless you wish to change the way
 * they work).
 * </p>
 *
 * <p>
 * The sorting method that this interface defines requires that the array
 * of provided objects all be of the same type, and that they implement
 * the {@link Comparable Comparable} interface.  This means that all members of
 * the array must support the {@linkplain Comparable#compareTo compareTo()}
 * method.  You should implement your sort using
 * {@linkplain Comparable#compareTo compareTo()} instead of the relational
 * operators &lt;, &gt; and ==.
 * </p>
 *
 * @author Jason Healy
 *
 */
public interface Sorter {

    /**
     * <p>
     * Performs an in-place sort of the specified range of indices in the
     * specified array.  Only the items in the given range are sorted, and
     * the range is from min (inclusive) to max (exclusive).  If min &gt;= max
     * then the range to sort is empty and the method does nothing.  The array
     * is sorted in-place (items are reassigned to different positions within
     * the array) and so no copy of the array is made or returned.
     * </p>
     *
     * <p>
     * Items are sorted in ascending order according to their natural ordering.
     * That is, from smallest to largest according to each item's
     * {@linkplain Comparable#compareTo compareTo()} method.  The array after
     * the method completes should have the following property hold true for
     * all k from min .. max-1:
     * </p>
     *
     * <p>
     * a[k].compareTo(a[k+1]) &lt; 1
     * </p>
     *
     * <p>
     * <b>Precondition: all items in a are {@linkplain Comparable#compareTo comparable};
     * min &gt;= 0;
     * max &lt;= a.length</b>
     * </p>
     *
     * <p>
     * <b>Postcondition: all items with indices min to max-1 are in ascending
     * order; items outside this range are not modified</b>
     * </p>
     *
     * @param <T> Any type that implements the {@link Comparable Comparable} interface
     * @param a The array to sort
     * @param min The smallest index in the array to consider during sorting
     * @param max The first index in the array that is larger than min that
     * should <b>not</b> be considered during sorting (max-1 is the highest
     * index that is part of the sort).
     */
    <T extends Comparable<? super T>> void sort(T[] a, int min, int max);


    /**
     * <p>
     * Same as the ranged version of sort, except that the entire array
     * is sorted (no range is necessary).  This method invokes the ranged
     * version with 0 as its minimum index and a.length as its maximum index.
     * </p>
     *
     * <p>
     * <b>Precondition: all items in a are {@linkplain Comparable#compareTo comparable}</b>
     * </p>
     *
     * <p>
     * <b>Postcondition: all items in the array are in ascending order</b>
     * </p>
     *
     * @param <T> Any type that implements the {@link Comparable Comparable} interface
     * @param a An array of items to sort
     *
     * @see #sort(T[], int, int)
     */
    default <T extends Comparable<? super T>> void sort(T[] a) {
        sort(a, 0, a.length);
    }


    /**
     * <p>
     * Utility method to assist with reordering items in an array.  This method
     * performs an in-place swap such that the items at the two given indices
     * trade places in the array.
     * </p>
     *
     * <p>
     * <b>Precondition: m and n are valid indices in the array (that is,
     * 0 &lt;= index &lt; a.length)</b>
     * </p>
     *
     * <p>
     * <b>Postcondition: a[m] contains the item formerly at a[n] and vice-versa</b>
     * </p>
     *
     * @param <T> Any type that implements the {@link Comparable Comparable} interface
     * @param a The array to modify
     * @param m The index of the first item to swap
     * @param n The index of the second item to swap
     */
    default <T extends Comparable<? super T>> void swap(T[] a, int m, int n) {
        T temp = a[m];  // save the first item
        a[m] = a[n];    // overwrite first with second
        a[n] = temp;    // overwrite second with saved first
    }


    /**
     * <p>
     * Utility method to assist with debugging a sort. This method dumps out
     * the elements in the given range of indices to the console for inspection.
     * </p>
     *
     * @param <T> Any type that implements the {@link Comparable Comparable}
     *            interface
     * @param a   The array to print
     * @param min The smallest index in the array to print
     * @param max The first index in the array that is larger than min that
     *            should <b>not</b> be printed (max-1 is the highest index that is
     *            printed).
     */
    default <T extends Comparable<? super T>> String stringify(T[] a, int min, int max) {
        StringBuilder sb = new StringBuilder(256);
        sb.append("a = new ");
        sb.append(a.getClass().getComponentType().getSimpleName());
        sb.append("[");
        sb.append(a.length);
        sb.append("]");
        sb.append(";   min:");
        sb.append(min);
        sb.append(", max:");
        sb.append(max);
        sb.append("    ---\n");

        for (int i = min; i < max; i++) {
            sb.append(" [");
            sb.append(i);
            sb.append("]=");
            sb.append(a[i]);
        }

        if (max - min <= 0) {
            sb.append(" [no items in range]");
        }

        return sb.toString();
    }

    /**
     * <p>
     * Same as the ranged version of dump, except that the entire array
     * is printed (no range is necessary). This method invokes the ranged
     * version with 0 as its minimum index and a.length as its maximum index.
     * </p>
     *
     * @param <T> Any type that implements the {@link Comparable Comparable}
     *            interface
     * @param a   An array of items to print
     *
     * @see #dump(T[], int, int)
     */
    default <T extends Comparable<? super T>> String stringify(T[] a) {
        return stringify(a, 0, a.length);
    }

    /**
     * <p>
     * Utility method to assist with debugging a sort. This method dumps out
     * the elements in the given range of indices to the console for inspection.
     * </p>
     *
     * @param <T> Any type that implements the {@link Comparable Comparable}
     *            interface
     * @param a   The array to print
     * @param min The smallest index in the array to print
     * @param max The first index in the array that is larger than min that
     *            should <b>not</b> be printed (max-1 is the highest index that is
     *            printed).
     */
    default <T extends Comparable<? super T>> void dump(T[] a, int min, int max) {
        System.out.println(stringify(a, min, max));
    }

    /**
     * <p>
     * Same as the ranged version of dump, except that the entire array
     * is printed (no range is necessary). This method invokes the ranged
     * version with 0 as its minimum index and a.length as its maximum index.
     * </p>
     *
     * @param <T> Any type that implements the {@link Comparable Comparable}
     *            interface
     * @param a   An array of items to print
     *
     * @see #dump(T[], int, int)
     */
    default <T extends Comparable<? super T>> void dump(T[] a) {
        dump(a, 0, a.length);
    }

}
