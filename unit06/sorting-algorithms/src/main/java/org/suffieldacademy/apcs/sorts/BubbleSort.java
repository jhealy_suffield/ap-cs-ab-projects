package org.suffieldacademy.apcs.sorts;

/**
 * <p>
 * <code>BubbleSort</code> sorts an array of comparable items using the
 * BubbleSort algorithm.  Consecutive pairs of elements are swapped until
 * the array is in order.
 * </p>
 *
 * <p>
 * Students can use this file as a template to create their own sorting
 * classes that they can test automatically.
 * </p>
 * 
 * <p>
 * In general, you only need to override the "sort(a, min, max)" method.
 * Don't forget to put your name in the author tag!
 * </p>
 *
 * @author Jason Healy
 *
 */
public class BubbleSort implements Sorter {

    /* (non-Javadoc)
     * @see org.suffieldacademy.apcs.sorts.Sorter#sort(T[], int, int)
     */
    @Override
    public <T extends Comparable<? super T>> void sort(T[] a, int min, int max) {
        boolean done = false;
        // keep looping over the array until its sorted
        while (!done) {
            done = true; // initially assume the array is in order
            for (int i = min + 1; i < max; i++) {
                // Note the use of "compareTo" here.  Remember that we can use
                // compareTo() with a comparison to zero as a substitute for
                // traditional numeric comparisons:
                // if a.compareTo(b) > 0, then a "is greater than" b
                // if a.compareTo(b) < 0, then a "is less than" b
                // if a.compareTo(b) == 0, then a "equals" b
                if (a[i].compareTo(a[i-1]) < 0) { // two items are out of order
                    swap(a, i, i-1); // so swap them (using library method for swap)
                    done = false; // a swap was made, so we can't stop yet
                }
            }
            // debugging; use library methods
            System.out.println("Finished pass");
            dump(a, min, max);
        }
    }

}
