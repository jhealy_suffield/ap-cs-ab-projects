package org.suffieldacademy.apcs.sorts;

/**
 * This is the default Maven class.  You can add code here if you want,
 * but I've set up a JUnit test that's probably easier to use.
 *
 */
public class App
{
    public static void main( String[] args )
    {
        System.out.println("Don't run this class directly; instead use\n");
        System.out.println("  mvn test\n");
        System.out.println("To run a test harness on your code");
    }
}
