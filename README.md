# AP CS AB Projects

A repository for homework assignments in Mr. Healy's AP Computer Science A(B) course.  I will publish assignments in this repository, and students will make their own edits to the files and push them to their own repositories for me to reivew.

## Initial Setup Instructions

- "Fork" this project by clicking the fork icon at the top of the page
- Give a name to your fork, and add it to your namespace
- Make the fork private
- Add me as a "Reporter" to your forked project, so I can get your changes
- Clone your fork to your local machine

Once cloned, you'll also want to add my repository as your "upstream", so you can fetch updates as I make them to the projects.  "cd" into your repository folder and execute the following:

```
git remote add upstream https://gitlab.com/jhealy_suffield/ap-cs-ab-projects.git
```

Then, if you run:

```
git remote -v
```

You should see lines for both your ("origin") and my ("upstream") repositories.  If so, you're ready to go!

## Fetching and Merging Updates

I will make changes to my repository during the year.  Because your repository is a "fork" of mine, you must periodically fetch and merge my changes into your repository.  You can perform both of these operations at once on the command line by running the following in your Visual Studio Code terminal window:

```
git fetch upstream && git merge --no-edit upstream/main
```

You may also perform these steps in Visual Studio Code by running the Git menu items "Pull, Push > Fetch From All Remotes", followed by "Branch > Merge" and chosing "upstream/main".  You may need to accept the default merge message (you don't need to edit or change it).  Because of this, the command-line version may be easier.

## Submitting Assignments

To turn in your assigments to me, you must make sure your work is committed **and pushed** to your repository so I can see it.  Then, you send me the commit ID so I know what to view.

A quick checklist when you turn in assignments:

1. Is your work commented, particularly with an "@author" tag in the main comment?

2. Run the following:
```
git show --no-patch HEAD origin/HEAD
```
That will show your last commit on your computer along with the last commit pushed to your repository.  **They should be the same** (if they aren't, then you probably need to "push" so that all your local commits are in the online repository).

3. Copy the commit hash displayed above and submit it to me in Schology.  I will use the hash to pull the exact version of your code for review.

4. If you make additional changes after submitting to Schoology and want me to see them, please re-commit, re-push, and submit the new hash value.  It would also be great to include a quick note about what changed and why.

