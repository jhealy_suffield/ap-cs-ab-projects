/**
 * Recursion homework: write a recursive method that prints all the anagrams
 * of a given string.
 *
 * @author Your Name
 */
public class Anagram {

    public static void main(String[] args) {

    }

    public static void anagram(String s) {
        // You are permitted to use iteration (loops) in this method to
        // work through all the letters in the string.  Then, you can
        // call your recursive helper method from within the loop.
    }

    // Hint: you may want to specify an overloaded version of anagram()
    // that takes additional parameters, and makes the problem easier to
    // solve.  Think about the the problem in terms of what is "done" and
    // what is "left".  How can you define the anagram, and use those as
    // your parameters?

}
