/**
 * <p>
 * <b>NPCompleteAppetizers</b> comes from an XKCD cartoon
 * (http://xkcd.com/287/) where the speaker asks a restaurant
 * server to find a combination of appetizers that adds up
 * to $15.05.  This program attempts to solve that problem.
 * </p>
 *
 * @author Your Name
 */
public class NPCompleteAppetizers {

    /** Names of appetizers */
    public static final String[] NAME =
    {
        "Mixed Fruit",
        "French Fries",
        "Side Salad",
        "Hot Wings",
        "Mozzarella Sticks",
        "Sampler Plate"
    };

    /** Prices of appetizers (in cents, to avoid rounding error) */
    public static final int[] COST =
    {
        215,
        275,
        335,
        355,
        420,
        580
    };



    /**
     * <p>
     * Recursive method to determine if one (or more) of the appetizers
     * defined in the NAME and COST arrays can be purchased in exactly
     * the given amount of money.
     * </p>
     *
     * <p>
     * It is legal to purchase more than one of a given item to use
     * the available funds.
     * </p>
     *
     * <p>
     * For simplicity, we'll consider different orderings of the same
     * items to be unique (don't worry about de-duplicating different
     * permutations of items).
     * </p>
     *
     * <p>
     * The method should build a list of "itemsSoFar" as it goes.  Imagine
     * that each recursive call adds a single item to the list and
     * decrements from the total funds available.  If the funds get to
     * zero, then whatever is in "itemsSoFar" is a correct list of items.
     * If the funds aren't at zero yet, choose another item and recurse.
     * </p>
     *
     * @param available Money (in cents) available to spend
     * @param itemsSoFar A string containing all the items already "purchased"
     */
    public static void appetizers(int available, String itemsSoFar) {
        // You MAY use loops inside of this method to work through the
        // arrays of items and costs.  However, you must use recursion
        // as part of your general solution (indeed, you'll find it quite
        // difficult if you avoid using recursion entirely).
    }


    public static void main(String[] args) {
        // kick off the appetizers method with $15.05 to spend
        appetizers(1505, "");
    }


} // end of class NPCompleteAppetizers
