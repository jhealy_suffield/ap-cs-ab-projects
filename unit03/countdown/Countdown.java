/**
 * A program that counts down from a value to 1 recursively.
 *
 * @author Your Name
 */
public class Countdown {

    public static void main(String[] args) {
        countDown(10);
    }


    /**
     * Counts down from the given number to 1 (inclusive of both), printing
     * each on a line by itself.  No value is returned.
     *
     * This method MUST use recursion only to perform the countdown (no loops)!
     */
    public static void countDown(int n) {
    }
}
