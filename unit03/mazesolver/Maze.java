/**
 * Enumeration class for the possible values for each location in a Maze.
 * Others may wish to use this class to inspect a maze and determine what
 * is in the location they're looking in.
 *
 * @author Jason Healy
 * @internal Not student edited
 */
public enum Maze {

    /** Represents a wall in the maze that cannot be crossed */
    WALL('+'),

    /** Represents an unvisited space that can be used as part of a solution path */
    SPACE(' '),

    /** Represents the beginning location in the maze */
    BEGIN('B'),

    /** Represents the ending location that the program tries to reach */
    END('E'),

    /** Represents a space that has already been visited by the solver */
    VISITED('@'),

    /** Represents a space that was not understood when read in from a file */
    INVALID('I');


    /** String representation of this constant (used for maze input and output) */
    public final char VALUE;


    /**
     * Constructs a new Maze enumerator with the given char as its value.
     *
     * @param c The char to use as the value for this enumerator
     */
    Maze(char c) {
        VALUE = c;
    }


    /**
     * Converts this enumerator to it's value plus whitespace.
     *
     * @return String The string value of this enumerator
     */
    public String toString() {
        return " "+VALUE;
        //return ""+VALUE+VALUE;
    }


    /**
     * Utility method to convert chars into enumerated types.  Given
     * a char, converts to a proper enum type.
     *
     * @param c The char to convert
     *
     * @return Maze A Maze enum type equivalent to the char
     */
    public static Maze decode(char c) {
        for (Maze m : Maze.values()) {
            if (m.VALUE == c) {
                return m;
            }
        }

        System.out.println("Warning: invalid maze char: '" + c + "'");
        return INVALID;
    }

}
