import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;


/**
 * MazeSolver bootstraps the maze-solving program by reading in
 * a maze description file and running the Solver.solve() method.
 * Students do not need to edit this class, but may wish to use
 * the public methods provided for console output.
 *
 * @author Jason Healy
 * @internal Not student edited
 */
public class MazeSolver {

    /** Pointer to system input, or file input */
    private static BufferedReader input = null;


    /**
     * Clears the console window of output.
     */
    public static void clear() {
        System.out.println("\033[2J");
        System.out.flush();
    }


    /**
     * Causes the program to suspend execution so output doesn't go
     * by too quickly.  Optionally, will suspend until user presses
     * the "return" key.
     *
     * @param time Milliseconds to sleep for.  If < 0,
     *             wait for the user to press a key
     */
    public static void pause(int time) {
        if (time < 0) {
            if (input != null) {
                System.out.println("Press 'return' to continue...");
                try {
                    input.readLine();
                }
                catch (Exception e) {
                    // just give up on keyboard input from here on out
                    input = null;
                }
            }
        }
        else {
            try {Thread.sleep(time);} catch (Exception e) {}
        }
    }


    /**
     * Prints maze (with walled border) out to the console.
     *
     * @param m The 2D Maze object to print out.
     */
    public static void showMaze(Maze[][] m) {
        // top wall
        for (int j = -2; j < m[0].length; j++) {
            System.out.print(Maze.WALL);
        }
        System.out.println("");

        for (int i = 0; i < m.length; i++) {
            System.out.print(Maze.WALL); // left wall
            for (int j = 0; j < m[i].length; j++) {
                System.out.print(m[i][j]);
            }
            System.out.println(Maze.WALL); // right wall
        }

        // bottom wall
        for (int j = -2; j < m[0].length; j++) {
            System.out.print(Maze.WALL);
        }
        System.out.println("");
    }


    /**
     * Main method of program.  Prompts user to enter maze input, then reads
     * and validates the maze.  Once input, launches the Solver.solve()
     * method to attempt to solve the maze, and prints the results.
     *
     * @param args Command-line arguments
     */
    public static void main(String[] args) {

        InputStream is = System.in;

        if (args.length == 1) {
            try {
                is = new FileInputStream(args[0]);
                System.out.println("Reading maze from file '" + args[0] + "'");
            }
            catch (FileNotFoundException fnfe) {
                System.out.println("Unable to open file '" + args[0] + "': " +
                                   fnfe);
            }
        }
        else {
            System.out.println("Please provide maze file on the console:");
            System.out.println("  Line 1: Number of rows (integer)");
            System.out.println("  Line 2: Number of columnes (integer)");
            System.out.println("  Remaining lines: Maze descriptions");
            System.out.println("           Wall: '" + Maze.WALL + "'");
            System.out.println("          Space: '" + Maze.SPACE + "'");
            System.out.println("    Begin Point: '" + Maze.BEGIN + "'");
            System.out.println("      End Point: '" + Maze.END + "'");
            System.out.println("");
        }

        input = new BufferedReader(new InputStreamReader(is));

        Maze[][] maze = null;
        int beginRow = -1;
        int beginCol = -1;

        try {
            int rows = Integer.parseInt(input.readLine());
            int cols = Integer.parseInt(input.readLine());

            maze = new Maze[rows][cols];

            for (int r = 0; r < rows; r++) {
                for (int c = 0; c < cols; c++) {
                    maze[r][c] = Maze.decode((char)input.read());
                    if (maze[r][c] == Maze.BEGIN) {
                        beginRow = r;
                        beginCol = c;
                    }
                }
                input.read(); // newline
            }

        }
        catch (Exception e) {
            System.err.println("Could not read in maze: " + e);
        }

        // un-label the beginning
        maze[beginRow][beginCol] = Maze.SPACE;

        if (beginRow < 0 || beginCol < 0) {
            clear();
            System.err.println("Maze must have one begin location (" +
                               Maze.BEGIN.VALUE + ")");
        }
        else if (Solver.solve(maze, beginRow, beginCol)) {
            clear();
            System.out.println("I found a path!");
        }
        else {
            clear();
            System.out.println("No solution found!");
        }

        // re-label the beginning
        maze[beginRow][beginCol] = Maze.BEGIN;
        showMaze(maze);
    }

}
