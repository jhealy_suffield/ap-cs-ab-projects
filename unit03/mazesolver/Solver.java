/**
 * Class to solve a maze.  The only required method is the solve()
 * method, which recursively determines if the maze is solvable or
 * not.  Note that you may wish to use the public methods of the
 * MazeSolver class to help with terminal output.
 *
 * @author Your Name
 */
public class Solver {

    /**
     * Given a starting location in a maze, attempts to find a solution
     * to the maze recursively.  Returns true if this method (or
     * one of its recursive invocations) was able to find a path
     * to the end point.  If this method's location is on the path to
     * a solution, it should mark it as "visited"; otherwise, it should
     * leave the space unchanged.
     *
     * @param maze The maze object to solve
     * @param row The row of the cell to begin the search from
     * @param col The column of the cell to begin the search from
     *
     * @return boolean True, if this location is on the path to
     *                 a solution for the maze.
     */
    public static boolean solve(Maze[][] maze, int row, int col) {

        // You'll need to use the Maze enumeration class to determine
        // what each location in the 2D array holds.
        //
        // The Enumeration values can be accessed using their name,
        // like this:
        //
        // assignment: maze[row][col] = Maze.WALL;
        // test: if (maze[row][col] == MAZE.SPACE) { ... }

        // Also, you may find the public methods of the MazeSolver
        // class helpful in displaying your maze and pausing the
        // output.

        return false;
    }

}
