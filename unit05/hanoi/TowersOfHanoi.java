import java.util.Scanner;


/**
 * Console-only version of the Towers of Hanoi problem.  This program
 * asks the users for the number of discs they'd like to solve for, and
 * then prints out a sequence of moves to solve the puzzle.
 *
 * @author Your Name
 */
public class TowersOfHanoi {

    /**
     * Main method.  Prompt the user for the number of discs, and then
     * kick off the recursive solver.  Print a total number of moves
     * when complete.
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int discs = 0;

        // get the actual number of discs from the user here
        // (don't let them enter an invalid number!)

        // then call the recursive hanoi method

        // then print out the number of moves
    }


    /**
     * Towers of Hanoi solver.  Given a number of discs to
     * solve for, and the label for the starting, ending, and temporary
     * (non-start and non-end) towers, this method prints a sequence of
     * moves while keeping track of the total number of moves made.
     *
     * PRECONDITION: d is not negative
     *
     * @param d The number of discs to solve for (greater than zero)
     * @param start The name of the tower that is the starting point
     * @param end The name of the tower that is the ending point
     * @param temp The name of the tower that is neither the start
     *             nor the end (you can move discs out of the way to here)
     *
     * @return int The number of moves necessary to solve the puzzle
     */
    public static int hanoi(int d, String start, String end, String temp) {
        return -1; // fix this
    }
}

