/**
 * Binary Search implementation homework.
 *
 * @author Your Name
 */
public class BinarySearch {

    /**
     * Iterative Binary Search
     *
     * Searches through the given sorted array for the given target value.
     * If found, it returns the index of the target value.
     * If the value does not exist in the array, -1 is returned.
     *
     * @param a An array of integers sorted in ascending order
     * @param t The target value to search for
     *
     * @return the index of t in the array, or -1 if not found
     *
     * Precondition: The array is sorted in ascending order
     */
    public static int ibs(int[] a, int t) {
        return -1; // erase this line and replace with your code
    }


    /**
     * Recursive Binary Search
     *
     * Searches through the given sorted array for the given target value.
     * If found, it returns the index of the target value.
     * If the value does not exist in the array, -1 is returned.
     * The search only takes place between the indices "min" (inclusive)
     * and "max" (exclusive).  The initial recursive call would therefore be
     *   rbs(a, t, 0, a.length);
     *
     * NO LOOPS ARE ALLOWED IN THIS METHOD (only recursion)
     *
     * NOTE THAT a[max] IS NOT PART OF THE SEARCH; the smallest index
     * you inspect is min, and the largest index you inspect is max-1.
     *
     * @param a An array of integers sorted in ascending order
     * @param t The target value to search for
     * @param min The smallest index to search from (inclusive)
     * @param max The largest index to search to (exclusive)
     *
     * @return the index of t in the array, or -1 if not found
     * in the interval [min, max) of the array
     *
     * Precondition: The array is sorted in ascending order,
     *               0 <= min <= a.length,
     *               0 <= max <= a.length,
     */
    public static int rbs(int[] a, int t, int min, int max) {
        return -1; // erase this line and replace with your code
    }

    public static void main(String[] args) {
        // run and test your code here; you can make an int array and
        // make sure that your code returns reasonable values
        int[] a = new int[] { 0, 2, 4, 6, 8, 10 };
        System.out.println(ibs(a, 4)); // should print "2"
    }
}
